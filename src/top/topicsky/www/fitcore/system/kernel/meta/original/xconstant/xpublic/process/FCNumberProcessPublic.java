package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCNumberPublic;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model.FCNumberModelPublic;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.function.Supplier;

import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;
import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.STCHAR;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 15 时 32 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FCNumberProcessPublic implements Serializable{
    /**
     * The Fc number model public golbal.
     */
    static Supplier<FCNumberModelPublic> fcNumberModelPublicGolbal=()->new FCNumberModelPublic();
    /**
     * The Clazz fc number public.
     */
    static Class<FCNumberPublic> clazzFCNumberPublic=FCNumberPublic.class;

    /**
     * Gets fc number public info.
     *
     * @param clazz
     *         the clazz
     *
     * @return the fc number public info
     *
     * @throws Exception
     *         the exception
     */
    public static FCNumberModelPublic getFCNumberPublicInfo(Class<?> clazz) throws Exception{
        FCNumberModelPublic fcNumberModelPublic=fcNumberModelPublicGolbal.get();
        Field[] fields=clazz.getDeclaredFields();
        for(Field field : fields){
            FCNumberPublic annotationFCNumberPublicField=field.getAnnotation(clazzFCNumberPublic);
            boolean annotationFCNumberPublicBoolean=field.isAnnotationPresent(clazzFCNumberPublic);
            Class fcNumberModelPublicClazz=fcNumberModelPublic.getClass();
            Method m1=fcNumberModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCNumberPublicBoolean){
                m1.invoke(fcNumberModelPublic,annotationFCNumberPublicField.numberExp());
            }
        }
        return fcNumberModelPublic;
    }

    /**
     * Gets fc number public info object.
     *
     * @param object
     *         the object
     *
     * @return the fc number public info object
     *
     * @throws Exception
     *         the exception
     */
    public static FCNumberModelPublic getFCNumberPublicInfoObject(Object object) throws Exception{
        FCNumberModelPublic fcNumberModelPublic=fcNumberModelPublicGolbal.get();
        Field[] fields=object.getClass().getDeclaredFields();
        for(Field field : fields){
            field.setAccessible(true);
            Object filedstemp=field.get(object);
            FCNumberPublic annotationFCNumberPublicField=field.getAnnotation(clazzFCNumberPublic);
            boolean annotationFCNumberPublicBoolean=field.isAnnotationPresent(clazzFCNumberPublic);
            Class fcNumberModelPublicClazz=fcNumberModelPublic.getClass();
            Method m1=fcNumberModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCNumberPublicBoolean){
                if(filedstemp!=null){
                    m1.invoke(fcNumberModelPublic,OTS.apply(filedstemp));
                }else{
                    m1.invoke(fcNumberModelPublic,annotationFCNumberPublicField.numberExp());
                }
            }else{
                if(filedstemp!=null){
                    m1.invoke(fcNumberModelPublic,OTS.apply(filedstemp));
                }
            }
        }
        return fcNumberModelPublic;
    }
}
