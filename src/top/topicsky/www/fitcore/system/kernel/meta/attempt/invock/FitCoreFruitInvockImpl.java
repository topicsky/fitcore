package top.topicsky.www.fitcore.system.kernel.meta.attempt.invock;

import top.topicsky.www.fitcore.system.kernel.meta.attempt.model.FitCoreAppleImpl;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.process.FitCoreFruitInfoProcess;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.attempt.invock
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 13 时 22 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreFruitInvockImpl{
    /**
     * Get init class fit core apple.
     *
     * @return the fit core apple
     */
    public static FitCoreAppleImpl getInitClass(){
        return FitCoreFruitInfoProcess.getFruitInfo(FitCoreAppleImpl.class);
    }

    ;

    /**
     * Gets init object.
     *
     * @param fitCoreAppleImpl
     *         the fit core apple
     *
     * @return the init object
     *
     * @throws Exception
     *         the exception
     */
    public static FitCoreAppleImpl getInitObject(FitCoreAppleImpl fitCoreAppleImpl) throws Exception{
        return FitCoreFruitInfoProcess.getFruitInfoObject(fitCoreAppleImpl);
    }

    ;
}
