package top.topicsky.www.fitcore.system.kernel.meta.custom.invock;

import top.topicsky.www.fitcore.system.kernel.meta.custom.model.FitCoreUserImpl;
import top.topicsky.www.fitcore.system.kernel.meta.custom.process.FitCoreUserProcessImpl;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.custom.invock
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 17 时 10 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreUserInvock{
    /**
     * Save string.
     *
     * @param fitCoreAppleImpl
     *         the fit core apple
     *
     * @return the string
     *
     * @throws Exception
     *         the exception
     */
    public static String save(FitCoreUserImpl fitCoreAppleImpl) throws Exception{
        return FitCoreUserProcessImpl.save(fitCoreAppleImpl);
    }

    ;

    /**
     * Update string.
     *
     * @param fitCoreAppleImpl
     *         the fit core apple
     *
     * @return the string
     *
     * @throws Exception
     *         the exception
     */
    public static String update(FitCoreUserImpl fitCoreAppleImpl) throws Exception{
        return FitCoreUserProcessImpl.update(fitCoreAppleImpl);
    }
}
