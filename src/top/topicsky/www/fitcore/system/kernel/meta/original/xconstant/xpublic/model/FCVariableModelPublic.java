package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCVariablePublic;

import java.io.Serializable;

import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicVariable.*;

/**
 * The type Fc variable model public.
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FCVariableModelPublic implements Serializable{
    @FCVariablePublic
    private String variableDafault;
    @FCVariablePublic(variableExp=SUCCESS)
    private String variableSUCCESS;
    @FCVariablePublic(variableExp=FAILURE)
    private String variableFAILURE;
    @FCVariablePublic(variableExp=INITVOIDSTATIC)
    private String variableINITVOIDSTATIC;
    @FCVariablePublic(variableExp=INITBASICSTATIC)
    private String variableINITBASICSTATIC;
    @FCVariablePublic(variableExp=INITNOPARAMSTATIC)
    private String variableINITNOPARAMSTATIC;
    @FCVariablePublic(variableExp=INITNORETURNSTATIC)
    private String variableINITNORETURNSTATIC;
    @FCVariablePublic(variableExp=INITVOIDDAFAULT)
    private String variableINITVOIDDAFAULT;
    @FCVariablePublic(variableExp=INITBASICDAFAULT)
    private String variableINITBASICDAFAULT;
    @FCVariablePublic(variableExp=INITNOPARAMDAFAULT)
    private String variableINITNOPARAMDAFAULT;
    @FCVariablePublic(variableExp=INITNORETURNDAFAULT)
    private String variableINITNORETURNDAFAULT;
    private String variableNoMetaSUCCESS;
    private String variableNoMetaFAILURE;
    private String variableNoMetaINITVOIDSTATIC;
    private String variableNoMetaINITBASICSTATIC;
    private String variableNoMetaINITNOPARAMSTATIC;
    private String variableNoMetaINITNORETURNSTATIC;
    private String variableNoMetaINITVOIDDAFAULT;
    private String variableNoMetaINITBASICDAFAULT;
    private String variableNoMetaINITNOPARAMDAFAULT;
    private String variableNoMetaINITNORETURNDAFAULT;

    @Override
    public String toString(){
        return "FCVariableModelPublic{"+
                "variableDafault='"+variableDafault+'\''+
                ", variableSUCCESS='"+variableSUCCESS+'\''+
                ", variableFAILURE='"+variableFAILURE+'\''+
                ", variableINITVOIDSTATIC='"+variableINITVOIDSTATIC+'\''+
                ", variableINITBASICSTATIC='"+variableINITBASICSTATIC+'\''+
                ", variableINITNOPARAMSTATIC='"+variableINITNOPARAMSTATIC+'\''+
                ", variableINITNORETURNSTATIC='"+variableINITNORETURNSTATIC+'\''+
                ", variableINITVOIDDAFAULT='"+variableINITVOIDDAFAULT+'\''+
                ", variableINITBASICDAFAULT='"+variableINITBASICDAFAULT+'\''+
                ", variableINITNOPARAMDAFAULT='"+variableINITNOPARAMDAFAULT+'\''+
                ", variableINITNORETURNDAFAULT='"+variableINITNORETURNDAFAULT+'\''+
                ", variableNoMetaSUCCESS='"+variableNoMetaSUCCESS+'\''+
                ", variableNoMetaFAILURE='"+variableNoMetaFAILURE+'\''+
                ", variableNoMetaINITVOIDSTATIC='"+variableNoMetaINITVOIDSTATIC+'\''+
                ", variableNoMetaINITBASICSTATIC='"+variableNoMetaINITBASICSTATIC+'\''+
                ", variableNoMetaINITNOPARAMSTATIC='"+variableNoMetaINITNOPARAMSTATIC+'\''+
                ", variableNoMetaINITNORETURNSTATIC='"+variableNoMetaINITNORETURNSTATIC+'\''+
                ", variableNoMetaINITVOIDDAFAULT='"+variableNoMetaINITVOIDDAFAULT+'\''+
                ", variableNoMetaINITBASICDAFAULT='"+variableNoMetaINITBASICDAFAULT+'\''+
                ", variableNoMetaINITNOPARAMDAFAULT='"+variableNoMetaINITNOPARAMDAFAULT+'\''+
                ", variableNoMetaINITNORETURNDAFAULT='"+variableNoMetaINITNORETURNDAFAULT+'\''+
                '}';
    }
}
