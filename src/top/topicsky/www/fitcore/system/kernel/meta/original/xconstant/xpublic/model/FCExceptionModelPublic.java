package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCExceptionPublic;

import java.io.Serializable;

import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicException.*;

/**
 * The type Fc exception model public.
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FCExceptionModelPublic implements Serializable{
    @FCExceptionPublic
    private String exceptionDefault;
    @FCExceptionPublic(exceptionExp=ARITHMETIC)
    private String exceptionARITHMETIC;
    @FCExceptionPublic(exceptionExp=ARRAYSTORE)
    private String exceptionARRAYSTORE;
    @FCExceptionPublic(exceptionExp=CLASSNOTFOUND)
    private String exceptionCLASSNOTFOUND;
    @FCExceptionPublic(exceptionExp=ILLEGALACCESS)
    private String exceptionILLEGALACCESS;
    @FCExceptionPublic(exceptionExp=ILLEGALMONITORSTATE)
    private String exceptionILLEGALMONITORSTATE;
    @FCExceptionPublic(exceptionExp=ILLEGALTHREADSTATE)
    private String exceptionILLEGALTHREADSTATE;
    @FCExceptionPublic(exceptionExp=INSTANTIATION)
    private String exceptionINSTANTIATION;
    @FCExceptionPublic(exceptionExp=NEGATIVEARRAYSIZE)
    private String exceptionNEGATIVEARRAYSIZE;
    @FCExceptionPublic(exceptionExp=NOSUCHMETHOD)
    private String exceptionNOSUCHMETHOD;
    @FCExceptionPublic(exceptionExp=NUMBERFORMAT)
    private String exceptionNUMBERFORMAT;
    @FCExceptionPublic(exceptionExp=RUNTIM)
    private String exceptionRUNTIM;
    @FCExceptionPublic(exceptionExp=STRINGINDEXOUTOFBOUNDS)
    private String exceptionSTRINGINDEXOUTOFBOUNDS;
    @FCExceptionPublic(exceptionExp=UNSUPPORTEDOPERATION)
    private String exceptionUNSUPPORTEDOPERATION;
    @FCExceptionPublic(exceptionExp=ARRAYINDEXOUTOFBOUNDS)
    private String exceptionARRAYINDEXOUTOFBOUNDS;
    @FCExceptionPublic(exceptionExp=CLASSCAST)
    private String exceptionCLASSCAST;
    @FCExceptionPublic(exceptionExp=CLONENOTSUPPORTED)
    private String exceptionCLONENOTSUPPORTED;
    @FCExceptionPublic(exceptionExp=ENUMCONSTANTNOTPRESENT)
    private String exceptionENUMCONSTANTNOTPRESENT;
    @FCExceptionPublic(exceptionExp=GLOBALEXCEPTIONHANDLERINTER)
    private String exceptionGLOBALEXCEPTIONHANDLERINTER;
    @FCExceptionPublic(exceptionExp=ILLEGALARGUMENT)
    private String exceptionILLEGALARGUMENT;
    @FCExceptionPublic(exceptionExp=ILLEGALSTATE)
    private String exceptionILLEGALSTATE;
    @FCExceptionPublic(exceptionExp=INDEXOUTOFBOUNDS)
    private String exceptionINDEXOUTOFBOUNDS;
    @FCExceptionPublic(exceptionExp=INTERRUPTED)
    private String exceptionINTERRUPTED;
    @FCExceptionPublic(exceptionExp=NOSUCHFIELD)
    private String exceptionNOSUCHFIELD;
    @FCExceptionPublic(exceptionExp=NULLPOINTER)
    private String exceptionNULLPOINTER;
    @FCExceptionPublic(exceptionExp=REFLECTIVEOPERATION)
    private String exceptionREFLECTIVEOPERATION;
    @FCExceptionPublic(exceptionExp=SECURITY)
    private String exceptionSECURITY;
    @FCExceptionPublic(exceptionExp=TYPENOTPRESENT)
    private String exceptionTYPENOTPRESENT;
    private String exceptionNoMetaDefault;
    private String exceptionNoMetaARITHMETIC;
    private String exceptionNoMetaARRAYSTORE;
    private String exceptionNoMetaCLASSNOTFOUND;
    private String exceptionNoMetaILLEGALACCESS;
    private String exceptionNoMetaILLEGALMONITORSTATE;
    private String exceptionNoMetaILLEGALTHREADSTATE;
    private String exceptionNoMetaINSTANTIATION;
    private String exceptionNoMetaNEGATIVEARRAYSIZE;
    private String exceptionNoMetaNOSUCHMETHOD;
    private String exceptionNoMetaNUMBERFORMAT;
    private String exceptionNoMetaRUNTIM;
    private String exceptionNoMetaSTRINGINDEXOUTOFBOUNDS;
    private String exceptionNoMetaUNSUPPORTEDOPERATION;
    private String exceptionNoMetaARRAYINDEXOUTOFBOUNDS;
    private String exceptionNoMetaCLASSCAST;
    private String exceptionNoMetaCLONENOTSUPPORTED;
    private String exceptionNoMetaENUMCONSTANTNOTPRESENT;
    private String exceptionNoMetaGLOBALEXCEPTIONHANDLERINTER;
    private String exceptionNoMetaILLEGALARGUMENT;
    private String exceptionNoMetaILLEGALSTATE;
    private String exceptionNoMetaINDEXOUTOFBOUNDS;
    private String exceptionNoMetaINTERRUPTED;
    private String exceptionNoMetaNOSUCHFIELD;
    private String exceptionNoMetaNULLPOINTER;
    private String exceptionNoMetaREFLECTIVEOPERATION;
    private String exceptionNoMetaSECURITY;
    private String exceptionNoMetaTYPENOTPRESENT;

    @Override
    public String toString(){
        return "FCExceptionModelPublic{"+
                "exceptionDefault='"+exceptionDefault+'\''+
                ", exceptionARITHMETIC='"+exceptionARITHMETIC+'\''+
                ", exceptionARRAYSTORE='"+exceptionARRAYSTORE+'\''+
                ", exceptionCLASSNOTFOUND='"+exceptionCLASSNOTFOUND+'\''+
                ", exceptionILLEGALACCESS='"+exceptionILLEGALACCESS+'\''+
                ", exceptionILLEGALMONITORSTATE='"+exceptionILLEGALMONITORSTATE+'\''+
                ", exceptionILLEGALTHREADSTATE='"+exceptionILLEGALTHREADSTATE+'\''+
                ", exceptionINSTANTIATION='"+exceptionINSTANTIATION+'\''+
                ", exceptionNEGATIVEARRAYSIZE='"+exceptionNEGATIVEARRAYSIZE+'\''+
                ", exceptionNOSUCHMETHOD='"+exceptionNOSUCHMETHOD+'\''+
                ", exceptionNUMBERFORMAT='"+exceptionNUMBERFORMAT+'\''+
                ", exceptionRUNTIM='"+exceptionRUNTIM+'\''+
                ", exceptionSTRINGINDEXOUTOFBOUNDS='"+exceptionSTRINGINDEXOUTOFBOUNDS+'\''+
                ", exceptionUNSUPPORTEDOPERATION='"+exceptionUNSUPPORTEDOPERATION+'\''+
                ", exceptionARRAYINDEXOUTOFBOUNDS='"+exceptionARRAYINDEXOUTOFBOUNDS+'\''+
                ", exceptionCLASSCAST='"+exceptionCLASSCAST+'\''+
                ", exceptionCLONENOTSUPPORTED='"+exceptionCLONENOTSUPPORTED+'\''+
                ", exceptionENUMCONSTANTNOTPRESENT='"+exceptionENUMCONSTANTNOTPRESENT+'\''+
                ", exceptionGLOBALEXCEPTIONHANDLERINTER='"+exceptionGLOBALEXCEPTIONHANDLERINTER+'\''+
                ", exceptionILLEGALARGUMENT='"+exceptionILLEGALARGUMENT+'\''+
                ", exceptionILLEGALSTATE='"+exceptionILLEGALSTATE+'\''+
                ", exceptionINDEXOUTOFBOUNDS='"+exceptionINDEXOUTOFBOUNDS+'\''+
                ", exceptionINTERRUPTED='"+exceptionINTERRUPTED+'\''+
                ", exceptionNOSUCHFIELD='"+exceptionNOSUCHFIELD+'\''+
                ", exceptionNULLPOINTER='"+exceptionNULLPOINTER+'\''+
                ", exceptionREFLECTIVEOPERATION='"+exceptionREFLECTIVEOPERATION+'\''+
                ", exceptionSECURITY='"+exceptionSECURITY+'\''+
                ", exceptionTYPENOTPRESENT='"+exceptionTYPENOTPRESENT+'\''+
                ", exceptionNoMetaDefault='"+exceptionNoMetaDefault+'\''+
                ", exceptionNoMetaARITHMETIC='"+exceptionNoMetaARITHMETIC+'\''+
                ", exceptionNoMetaARRAYSTORE='"+exceptionNoMetaARRAYSTORE+'\''+
                ", exceptionNoMetaCLASSNOTFOUND='"+exceptionNoMetaCLASSNOTFOUND+'\''+
                ", exceptionNoMetaILLEGALACCESS='"+exceptionNoMetaILLEGALACCESS+'\''+
                ", exceptionNoMetaILLEGALMONITORSTATE='"+exceptionNoMetaILLEGALMONITORSTATE+'\''+
                ", exceptionNoMetaILLEGALTHREADSTATE='"+exceptionNoMetaILLEGALTHREADSTATE+'\''+
                ", exceptionNoMetaINSTANTIATION='"+exceptionNoMetaINSTANTIATION+'\''+
                ", exceptionNoMetaNEGATIVEARRAYSIZE='"+exceptionNoMetaNEGATIVEARRAYSIZE+'\''+
                ", exceptionNoMetaNOSUCHMETHOD='"+exceptionNoMetaNOSUCHMETHOD+'\''+
                ", exceptionNoMetaNUMBERFORMAT='"+exceptionNoMetaNUMBERFORMAT+'\''+
                ", exceptionNoMetaRUNTIM='"+exceptionNoMetaRUNTIM+'\''+
                ", exceptionNoMetaSTRINGINDEXOUTOFBOUNDS='"+exceptionNoMetaSTRINGINDEXOUTOFBOUNDS+'\''+
                ", exceptionNoMetaUNSUPPORTEDOPERATION='"+exceptionNoMetaUNSUPPORTEDOPERATION+'\''+
                ", exceptionNoMetaARRAYINDEXOUTOFBOUNDS='"+exceptionNoMetaARRAYINDEXOUTOFBOUNDS+'\''+
                ", exceptionNoMetaCLASSCAST='"+exceptionNoMetaCLASSCAST+'\''+
                ", exceptionNoMetaCLONENOTSUPPORTED='"+exceptionNoMetaCLONENOTSUPPORTED+'\''+
                ", exceptionNoMetaENUMCONSTANTNOTPRESENT='"+exceptionNoMetaENUMCONSTANTNOTPRESENT+'\''+
                ", exceptionNoMetaGLOBALEXCEPTIONHANDLERINTER='"+exceptionNoMetaGLOBALEXCEPTIONHANDLERINTER+'\''+
                ", exceptionNoMetaILLEGALARGUMENT='"+exceptionNoMetaILLEGALARGUMENT+'\''+
                ", exceptionNoMetaILLEGALSTATE='"+exceptionNoMetaILLEGALSTATE+'\''+
                ", exceptionNoMetaINDEXOUTOFBOUNDS='"+exceptionNoMetaINDEXOUTOFBOUNDS+'\''+
                ", exceptionNoMetaINTERRUPTED='"+exceptionNoMetaINTERRUPTED+'\''+
                ", exceptionNoMetaNOSUCHFIELD='"+exceptionNoMetaNOSUCHFIELD+'\''+
                ", exceptionNoMetaNULLPOINTER='"+exceptionNoMetaNULLPOINTER+'\''+
                ", exceptionNoMetaREFLECTIVEOPERATION='"+exceptionNoMetaREFLECTIVEOPERATION+'\''+
                ", exceptionNoMetaSECURITY='"+exceptionNoMetaSECURITY+'\''+
                ", exceptionNoMetaTYPENOTPRESENT='"+exceptionNoMetaTYPENOTPRESENT+'\''+
                '}';
    }
}
