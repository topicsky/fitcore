package top.topicsky.www.fitcore.system.kernel.meta.attempt;

import top.topicsky.www.fitcore.system.kernel.meta.attempt.invock.FitCoreFruitInvockImpl;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.model.FitCoreAppleImpl;
import top.topicsky.www.fitcore.system.kernel.meta.attempt.process.FitCoreFruitInfoProcess;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;
import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.attempt
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 11 时 10 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreTest{
    /**
     * The entry point of application.
     *
     * @param args
     *         the input arguments
     *
     * @throws Exception
     *         the exception
     */
    public static void main(String[] args) throws Exception{
        SOPL.accept(OTS.apply(FitCoreFruitInvockImpl.getInitClass()));
        /*---------------------------------------------------------------------------------------------------*/
        FitCoreAppleImpl fitCoreAppleImplX=new FitCoreAppleImpl();
        fitCoreAppleImplX.setAppleName("梨子");
        fitCoreAppleImplX.setAppleColor("YELLOW");
        SOPL.accept(OTS.apply(FitCoreFruitInfoProcess.getFruitInfoObject(fitCoreAppleImplX)));
        /*---------------------------------------------------------------------------------------------------*/
        FitCoreAppleImpl fitCoreAppleImplY=new FitCoreAppleImpl();
        fitCoreAppleImplY.setAppleProvider("供应商编号[1]，供应商名称[线下社区]，供应商地址[天上人间]");
        SOPL.accept(OTS.apply(FitCoreFruitInfoProcess.getFruitInfoObject(fitCoreAppleImplY)));
        /*---------------------------------------------------------------------------------------------------*/
        SOPL.accept(OTS.apply(FitCoreFruitInvockImpl.getInitObject(new FitCoreAppleImpl())));
        /*---------------------------------------------------------------------------------------------------*/
    }
}
