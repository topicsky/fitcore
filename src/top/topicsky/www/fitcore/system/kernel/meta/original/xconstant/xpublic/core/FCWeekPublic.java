package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core;

import java.lang.annotation.*;

import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicWeek.SUNDAY;
/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 15 时 20 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */

/**
 * 1.所有基本数据类型（int,float,boolean,byte,double,char,long,short)
 * 2.String类型
 * 3.Class类型
 * 4.enum类型
 * 5.Annotation类型
 * 6.以上所有类型的数组
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface FCWeekPublic{
    /**
     * Week exp string string.
     *
     * @return the string
     */
    String weekExpString() default SUNDAY;

    /**
     * Week exp int int.
     *
     * @return the int
     */
    int weekExpInt() default 0;

    /**
     * Week exp float float.
     *
     * @return the float
     */
    float weekExpFloat() default 0.0F;

    /**
     * Week exp boolean boolean.
     *
     * @return the boolean
     */
    boolean weekExpBoolean() default true;

    /**
     * Week exp byte byte.
     *
     * @return the byte
     */
    byte weekExpByte() default 0;

    /**
     * Week exp double double.
     *
     * @return the double
     */
    double weekExpDouble() default 0.0D;

    /**
     * Week exp char char.
     *
     * @return the char
     */
    char weekExpChar() default 'a';

    /**
     * Week exp long long.
     *
     * @return the long
     */
    long weekExpLong() default 0L;

    /**
     * Week exp short short.
     *
     * @return the short
     */
    short weekExpShort() default 0;

    /**
     * Week exp string list string [ ].
     *
     * @return the string [ ]
     */
    String[] weekExpStringList() default {SUNDAY};

    /**
     * Week exp int list int [ ].
     *
     * @return the int [ ]
     */
    int[] weekExpIntList() default {0};

    /**
     * Week exp float list float [ ].
     *
     * @return the float [ ]
     */
    float[] weekExpFloatList() default {0.0F};

    /**
     * Week exp boolean list boolean [ ].
     *
     * @return the boolean [ ]
     */
    boolean[] weekExpBooleanList() default {true};

    /**
     * Week exp byte list byte [ ].
     *
     * @return the byte [ ]
     */
    byte[] weekExpByteList() default {0};

    /**
     * Week exp double list double [ ].
     *
     * @return the double [ ]
     */
    double[] weekExpDoubleList() default {0.0D};

    /**
     * Week exp char list char [ ].
     *
     * @return the char [ ]
     */
    char[] weekExpCharList() default {'a'};

    /**
     * Week exp long list long [ ].
     *
     * @return the long [ ]
     */
    long[] weekExpLongList() default {0L};

    /**
     * Week exp short list short [ ].
     *
     * @return the short [ ]
     */
    short[] weekExpShortList() default {0};
}
