package top.topicsky.www.fitcore.system.kernel.modellibrary.structural.decorator;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.structural.decorator
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 28 日 10 时 27 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicDecoratorBeanImpl implements FitCoreBasicDecoratorInter, Serializable{
    private FitCoreBasicDecoratorInter fitCoreBasicDecoratorInter;

    /**
     * Instantiates a new Fit core basic decorator bean.
     *
     * @param fitCoreBasicDecoratorInter
     *         the fit core basic decorator inter
     */
    public FitCoreBasicDecoratorBeanImpl(FitCoreBasicDecoratorInter fitCoreBasicDecoratorInter){
        super();
        this.fitCoreBasicDecoratorInter=fitCoreBasicDecoratorInter;
    }

    @Override
    public void fitCoreBasicDecorator(){
        this.before();
        fitCoreBasicDecoratorInter.fitCoreBasicDecorator();
        this.atfer();
    }

    private void before(){
        SOPL.accept("这是装饰类调用适配接口方法之前能操作的域");
    }

    private void atfer(){
        SOPL.accept("这是装饰类调用适配接口方法之后能操作的域");
    }
}
