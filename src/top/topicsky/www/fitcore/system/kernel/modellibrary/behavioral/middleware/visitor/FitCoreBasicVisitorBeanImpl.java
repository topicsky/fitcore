package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.middleware.visitor;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.middleware.visitor
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 30 日 11 时 25 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicVisitorBeanImpl implements FitCoreBasicVisitorInter, Serializable{
    @Override
    public void fitCoreBasicVisitor(FitCoreBasicSubjectInter fitCoreBasicSubjectInter){
        SOPL.accept("我是访问者，我访问的对象是："+fitCoreBasicSubjectInter.getFitCoreBasicSubject());
    }
}
