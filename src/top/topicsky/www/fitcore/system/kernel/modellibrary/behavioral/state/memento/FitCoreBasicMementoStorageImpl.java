package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.state.memento;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.state.memento
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 30 日 09 时 47 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <SI>
 *         the type parameter
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Transactional
public class FitCoreBasicMementoStorageImpl<SI> implements Serializable{
    private FitCoreBasicMementoMementoImpl fitCoreBasicMementoMementoImpl;

    /**
     * Instantiates a new Fit core basic memento storage.
     *
     * @param si
     *         the si
     */
    public FitCoreBasicMementoStorageImpl(SI si){
        SOPL.accept("这里初始化驱动，操作暂存类，也就是驱动类，构建（*Storage）");
        this.fitCoreBasicMementoMementoImpl=(FitCoreBasicMementoMementoImpl)si;
    }
}
