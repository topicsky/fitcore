package top.topicsky.www.fitcore.system.kernel.modellibrary.structural.adapter.object;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.structural.adapter.object
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 28 日 09 时 52 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */

/**
 * 当希望将一个对象转换成满足另一个新接口的对象时，可以创建一个Wrapper类，持有原类的一个实例，在Wrapper类的方法中，调用实例的方法就行。
 */
@Component
@Transactional
public interface FitCoreBasicAdapterObjectInter extends Serializable{
    /**
     * Fit core basic adapter object inherit.
     */
    public void fitCoreBasicAdapterObjectInherit();

    /**
     * Fit core basic adapter object inter.
     */
    public void fitCoreBasicAdapterObjectInter();
}
