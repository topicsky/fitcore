package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.state.state;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.state.state
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 30 日 10 时 52 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <SI>
 *         the type parameter
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FitCoreBasicStateCoreImpl<SI> implements Serializable{
    private SI si;

    /**
     * Fit core basic atate switch online.
     */
    public void FitCoreBasicAtateSwitchOnline(){
        SOPL.accept("这里切换到——在线——方式，调用一些联动操作");
    }

    /**
     * Fit core basic atate switch outline.
     */
    public void FitCoreBasicAtateSwitchOutline(){
        SOPL.accept("这里切换到——离线——方式，调用一些联动操作");
    }
}
