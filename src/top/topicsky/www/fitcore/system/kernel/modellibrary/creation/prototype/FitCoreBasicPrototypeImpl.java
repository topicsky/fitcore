package top.topicsky.www.fitcore.system.kernel.modellibrary.creation.prototype;

import java.io.*;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.creation.prototype
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 27 日 17 时 40 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreBasicPrototypeImpl implements Cloneable, Serializable{
    /**
     * 浅复制
     */
    @Override
    protected Object clone() throws CloneNotSupportedException{
        return super.clone();
    }

    /**
     * 深复制
     *
     * @return the object
     *
     * @throws IOException
     *         the io exception
     * @throws ClassNotFoundException
     *         the class not found exception
     */
    public Object deepClone() throws IOException, ClassNotFoundException{

        /* 写入当前对象的二进制流 */
        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        ObjectOutputStream oos=new ObjectOutputStream(bos);
        oos.writeObject(this);

        /* 读出二进制流产生的新对象 */
        ByteArrayInputStream bis=new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream ois=new ObjectInputStream(bis);
        return ois.readObject();
    }
}
