package top.topicsky.www.fitcore.system.kernel.modellibrary.structural.adapter.inter;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.structural.adapter.inter
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 28 日 10 时 10 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */

/**
 * 当不希望实现一个接口中所有的方法时，可以创建一个抽象类Wrapper，实现所有方法，我们写别的类的时候，继承抽象类即可。
 */
@Component
@Transactional
public interface FitCoreBasicAdapterInterInter extends Serializable{
    /**
     * Fit core basic adapter inter method xa.
     */
    public void fitCoreBasicAdapterInterMethod_XA();

    /**
     * Fit core basic adapter inter method xb.
     */
    public void fitCoreBasicAdapterInterMethod_XB();
}
