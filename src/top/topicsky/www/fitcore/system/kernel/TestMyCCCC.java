package top.topicsky.www.fitcore.system.kernel;


import top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.middleware.mediator.FitCoreBasicMediatorBeanImpl;
import top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.middleware.mediator.FitCoreBasicMediatorInter;

import java.util.Comparator;
import java.util.function.*;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 27 日 14 时 15 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class TestMyCCCC{
    /**
     * Main.
     *
     * @param args
     *         the args
     *
     * @throws Exception
     *         the exception
     */
    public static void main(String[] args) throws Exception{
        //Supplier<FitCoreBasicSimpleFactoryImpl> supplierSimple=()->new FitCoreBasicSimpleFactoryImpl();
        //Supplier<FitCoreBasicMultiFactoryImpl> supplierMulti=()->new FitCoreBasicMultiFactoryImpl();
        //supplierSimple.get().initSimple(INITSIMPLE).simpleInit("笨蛋");
        //supplierMulti.get().initMulti_Core_XA().multiInit("白痴");
        //supplierMulti.get().initMulti_Core_XB().multiInit("傻瓜");
        //FitCoreBasicStaticImpl.initMulti_Core_XA().multiInit("二哈");
        //FitCoreBasicStaticImpl.initMulti_Core_XB().multiInit("博美");
        //Supplier<FitCoreBasicAbstractImpl> supplierAbstract=()->new FitCoreBasicAbstractImpl();
        //supplierAbstract.get().abstractProduct().abstractInit("正是智商捉鸡");
        //Supplier<FitCoreBasicBuilderCaseImpl> supplierBuilder=()->new FitCoreBasicBuilderCaseImpl();
        //supplierBuilder.get().produceBasicBuilder(10);
        //builderList.stream().forEach(s->s.builderInit("这么多自己"));
        //Supplier<FitCoreBasicAdapterClazzBeanClazzImpl> supplierAdapterClazz=()->new FitCoreBasicAdapterClazzBeanClazzImpl();
        //supplierAdapterClazz.get().fitCoreBasicAdapterClazzInherit();
        //supplierAdapterClazz.get().fitCoreBasicAdapterClazzInter();
        //Supplier<FitCoreBasicAdapterObjectImpl> supplierAdapterObjectInherit=()->new FitCoreBasicAdapterObjectImpl();
        //Supplier<FitCoreBasicAdapterObjectBeanImpl> supplierAdapterObject=()->new FitCoreBasicAdapterObjectBeanImpl(supplierAdapterObjectInherit.get());
        //supplierAdapterObject.get().fitCoreBasicAdapterObjectInherit();
        //supplierAdapterObject.get().fitCoreBasicAdapterObjectInter();
        //Supplier<FitCoreBasicAdapterInherit_XA_Impl> supplierAdapterObjectInter_XA=()->new FitCoreBasicAdapterInherit_XA_Impl();
        //Supplier<FitCoreBasicAdapterInherit_XB_Impl> supplierAdapterObjectInter_XB=()->new FitCoreBasicAdapterInherit_XB_Impl();
        //supplierAdapterObjectInter_XA.get().fitCoreBasicAdapterInterMethod_XA();
        //supplierAdapterObjectInter_XA.get().fitCoreBasicAdapterInterMethod_XB();
        //supplierAdapterObjectInter_XB.get().fitCoreBasicAdapterInterMethod_XA();
        //supplierAdapterObjectInter_XB.get().fitCoreBasicAdapterInterMethod_XB();
        //Supplier<FitCoreBasicDecoratorImpl> supplierDecorator=()->new FitCoreBasicDecoratorImpl();
        //Supplier<FitCoreBasicDecoratorBeanImpl> supplierDecoratorBean=()->new FitCoreBasicDecoratorBeanImpl(supplierDecorator.get());
        //supplierDecoratorBean.get().fitCoreBasicDecorator();
        //Supplier<FitCoreBasicProxyImpl> supplierFitCoreBasicProxyImpl=()->new FitCoreBasicProxyImpl();
        //Supplier<FitCoreBasicProxyBeanImpl> supplierProxy=()->new FitCoreBasicProxyBeanImpl(supplierFitCoreBasicProxyImpl.get());
        //supplierProxy.get().fitCoreBasicProxy();
        //Supplier<FitCoreBasicFacadeApplicationImpl> supplierFacade=()->new FitCoreBasicFacadeApplicationImpl();
        //supplierFacade.get().initFacade();
        //supplierFacade.get().haltFacade();
        //Supplier<FitCoreBasicBridgeAbstractInheritImpl> supplierBridge=()->new FitCoreBasicBridgeAbstractInheritImpl();
        //Supplier<FitCoreBasicBridgeInter> supplierBridgeXA=()->new FitCoreBasicBridgeXAImpl();
        //Supplier<FitCoreBasicBridgeInter> supplierBridgeXB=()->new FitCoreBasicBridgeXBImpl();
        //FitCoreBasicBridgeAbstractInheritImpl fitCoreBasicBridgeAbstractInheritImpl=supplierBridge.get();
        //FitCoreBasicBridgeInter fitCoreBasicBridgeXA=supplierBridgeXA.get();
        //FitCoreBasicBridgeInter FitCoreBasicBridgeXB=supplierBridgeXB.get();
        //fitCoreBasicBridgeAbstractInheritImpl.setFitCoreBasicBridgeInter(fitCoreBasicBridgeXA);
        //fitCoreBasicBridgeAbstractInheritImpl.fitCoreBasicBridgeAbstractInit();
        //fitCoreBasicBridgeAbstractInheritImpl.setFitCoreBasicBridgeInter(FitCoreBasicBridgeXB);
        //fitCoreBasicBridgeAbstractInheritImpl.fitCoreBasicBridgeAbstractInit();
        //Supplier<FitCoreBasicFlyWeightConfImpl> supplierFlyWeight=()->new FitCoreBasicFlyWeightConfImpl();
        //FitCoreBasicFlyWeightImpl fitCoreBasicFlyWeightImpl=supplierFlyWeight.get().getConstructor();
        //Connection connectionXA=fitCoreBasicFlyWeightImpl.getConnection();
        //Connection connectionXB=fitCoreBasicFlyWeightImpl.getConnection();
        //Connection connectionXC=fitCoreBasicFlyWeightImpl.getConnection();
        //Connection connectionXD=fitCoreBasicFlyWeightImpl.getConnection();
        //Connection connectionXE=fitCoreBasicFlyWeightImpl.getConnection();
        //fitCoreBasicFlyWeightImpl.repatriate(connectionXA);
        //fitCoreBasicFlyWeightImpl.repatriate(connectionXB);
        //fitCoreBasicFlyWeightImpl.repatriate(connectionXC);
        //fitCoreBasicFlyWeightImpl.repatriate(connectionXD);
        //fitCoreBasicFlyWeightImpl.repatriate(connectionXE);
        //String expAddition="8.0+2.0";
        //String expSubtraction="8.0-2.0";
        //String expMultiplication="8.0*2.0";
        //String expDivision="8.0/2.0";
        //Supplier<FitCoreBasicStrategyInter> supplierAddition=()->new FitCoreBasicStrategyAdditionImpl();
        //Supplier<FitCoreBasicStrategyInter> supplierSubtraction=()->new FitCoreBasicStrategySubtractionImpl();
        //Supplier<FitCoreBasicStrategyInter> supplierMultiplication=()->new FitCoreBasicStrategyMultiplicationImpl();
        //Supplier<FitCoreBasicStrategyInter> supplierDivision=()->new FitCoreBasicStrategyDivisionImpl();
        //SOPL.accept(supplierAddition.get().fitCoreBasicStrategy(expAddition));
        //SOPL.accept(supplierSubtraction.get().fitCoreBasicStrategy(expSubtraction));
        //SOPL.accept(supplierMultiplication.get().fitCoreBasicStrategy(expMultiplication));
        //SOPL.accept(supplierDivision.get().fitCoreBasicStrategy(expDivision));
        //String expAddition="8.0+2.0";
        //String expSubtraction="8.0-2.0";
        //String expMultiplication="8.0*2.0";
        //String expDivision="8.0/2.0";
        //Supplier<FitCoreBasicTemplatementhodAbstractImpl> supplierAdditionTemplate=()->new FitCoreBasicTemplatementhodAdditionImpl();
        //Supplier<FitCoreBasicTemplatementhodAbstractImpl> supplierSubtractionTemplate=()->new FitCoreBasicTemplatementhodSubtractionImpl();
        //Supplier<FitCoreBasicTemplatementhodAbstractImpl> supplierMultiplicationTemplate=()->new FitCoreBasicTemplatementhodMultiplicationImpl();
        //Supplier<FitCoreBasicTemplatementhodAbstractImpl> supplierDivisionTemplate=()->new FitCoreBasicTemplatementhodDivisionImpl();
        //SOPL.accept(supplierAdditionTemplate.get().fitCoreBasicTemplatementhodAbstractInherit(expAddition,"\\+"));
        //SOPL.accept(supplierSubtractionTemplate.get().fitCoreBasicTemplatementhodAbstractInherit(expSubtraction,"\\-"));
        //SOPL.accept(supplierMultiplicationTemplate.get().fitCoreBasicTemplatementhodAbstractInherit(expMultiplication,"\\*"));
        //SOPL.accept(supplierDivisionTemplate.get().fitCoreBasicTemplatementhodAbstractInherit(expDivision,"\\/"));
        //Supplier<FitCoreBasicObserverSubjectInter> supplierSubject=()->new FitCoreBasicObserverSubjectAbstractBeanImpl();
        //Supplier<FitCoreBasicObserverInter> supplierObserverXA=()->new FitCoreBasicObserverXAImpl();
        //Supplier<FitCoreBasicObserverInter> supplierObserverXB=()->new FitCoreBasicObserverXBImpl();
        //FitCoreBasicObserverSubjectInter fitCoreBasicObserverSubjectAbstractBean=supplierSubject.get();
        //FitCoreBasicObserverInter fitCoreBasicObserverXA=supplierObserverXA.get();
        //FitCoreBasicObserverInter fitCoreBasicObserverXB=supplierObserverXB.get();
        //fitCoreBasicObserverSubjectAbstractBean.addObserver(fitCoreBasicObserverXA);
        //fitCoreBasicObserverSubjectAbstractBean.addObserver(fitCoreBasicObserverXB);
        //fitCoreBasicObserverSubjectAbstractBean.operation();
        //Supplier<FitCoreBasicIteratorCollectionInter> supplierIterator=()->new FitCoreBasicIteratorCollectionBeanImpl();
        //FitCoreBasicIteratorCollectionInter fitCoreBasicIteratorCollectionBean =supplierIterator.get();
        //String string[] = {"A","B","C","D","E"};
        //fitCoreBasicIteratorCollectionBean.setupObject(string);
        //FitCoreBasicIteratorCollectionIteratorInter fitCoreBasicIteratorCollectionIteratorInter=fitCoreBasicIteratorCollectionBean.fitCoreBasicIteratorCollectionIteratorInter();
        //while((fitCoreBasicIteratorCollectionIteratorInter.hasNext())){
        //    Object o=fitCoreBasicIteratorCollectionIteratorInter.obtainNext();
        //    SOPL.accept(OTS.apply(o));
        //}
        //Supplier<FitCoreBasicChainAbstractBeanImpl> supplierChainXA=()->new FitCoreBasicChainAbstractBeanImpl("老板");
        //Supplier<FitCoreBasicChainAbstractBeanImpl> supplierChainXB=()->new FitCoreBasicChainAbstractBeanImpl("老板娘");
        //Supplier<FitCoreBasicChainAbstractBeanImpl> supplierChainXC=()->new FitCoreBasicChainAbstractBeanImpl("小二");
        //FitCoreBasicChainAbstractBeanImpl supplierChainXABean = supplierChainXA.get();
        //FitCoreBasicChainAbstractBeanImpl supplierChainXBBean = supplierChainXB.get();
        //FitCoreBasicChainAbstractBeanImpl supplierChainXCBean = supplierChainXC.get();
        //supplierChainXABean.setFitCoreBasicChainInter(supplierChainXBBean);
        //supplierChainXBBean.setFitCoreBasicChainInter(supplierChainXCBean);
        //supplierChainXABean.fitCoreBasicChain();
        //Supplier<FitCoreBasicReceiverImpl> supplierCommandReceiver=()->new FitCoreBasicReceiverImpl();
        //Supplier<FitCoreBasicCommandInter> supplierCommand=()->new FitCoreBasicCommandImpl(supplierCommandReceiver.get());
        //Supplier<FitCoreBasicInvokerImpl> supplierCommandInvock=()->new FitCoreBasicInvokerImpl(supplierCommand.get());
        //supplierCommandInvock.get().FitCoreBasicInvoker();
        // //创建原始类
        //Supplier<FitCoreBasicMementoOriginalImpl> supplierOriginal=()->new FitCoreBasicMementoOriginalImpl("年轻真好");
        //FitCoreBasicMementoOriginalImpl fitCoreBasicMementoOriginal=supplierOriginal.get();
        //Supplier<FitCoreBasicMementoStorageImpl> supplierStorage=()->new FitCoreBasicMementoStorageImpl(fitCoreBasicMementoOriginal.createFitCoreBasicMementoMementoImpl());
        //FitCoreBasicMementoStorageImpl fitCoreBasicMementoStorage=supplierStorage.get();
        //// 修改原始类的状态
        //fitCoreBasicMementoOriginal.setSi("老了，不中用");
        //SOPL.accept("这是修改后的值："+fitCoreBasicMementoOriginal.getSi());
        //// 回复原始类的状态
        //fitCoreBasicMementoOriginal.restoreMemento(fitCoreBasicMementoStorage.getFitCoreBasicMementoMementoImpl());
        //SOPL.accept("这是还原后的值："+fitCoreBasicMementoOriginal.getSi());
        //Supplier<FitCoreBasicStateCoreImpl> supplierState=()->new FitCoreBasicStateCoreImpl();
        //FitCoreBasicStateCoreImpl fitCoreBasicStateCore =supplierState.get();
        //Supplier<FitCoreBasicAtateSwitchImpl> supplierStateSwitch=()->new FitCoreBasicAtateSwitchImpl(fitCoreBasicStateCore);
        //FitCoreBasicAtateSwitchImpl fitCoreBasicAtateSwitch =supplierStateSwitch.get();
        ////设置——在线——状态
        //fitCoreBasicStateCore.setSi("在线");
        //fitCoreBasicAtateSwitch.fitCoreBasicAtateSwitch();
        ////设置——离线——状态
        //fitCoreBasicStateCore.setSi("离线");
        //fitCoreBasicAtateSwitch.fitCoreBasicAtateSwitch();
        //Supplier<FitCoreBasicVisitorBeanImpl> supplierVisitor=()->new FitCoreBasicVisitorBeanImpl();
        //FitCoreBasicVisitorBeanImpl fitCoreBasicVisitorBean =supplierVisitor.get();
        //Supplier<FitCoreBasicSubjectBeanImpl> supplierSubject=()->new FitCoreBasicSubjectBeanImpl();
        //FitCoreBasicSubjectBeanImpl fitCoreBasicSubjectBean =supplierSubject.get();
        //fitCoreBasicSubjectBean.fitCoreBasicSubject(fitCoreBasicVisitorBean);
        Supplier<FitCoreBasicMediatorInter> supplierMediator=()->new FitCoreBasicMediatorBeanImpl();
        FitCoreBasicMediatorInter fitCoreBasicMediatorBean=supplierMediator.get();
        fitCoreBasicMediatorBean.createMediator();
        fitCoreBasicMediatorBean.allTakeEffect();
        //Supplier<FitCoreBasicContentImpl> supplierInterpreterContent=()->new FitCoreBasicContentImpl(11.0,8.0);
        //FitCoreBasicContentImpl fitCoreBasicContentImpl=supplierInterpreterContent.get();
        //Supplier<FitCoreBasicAdditionImpl> supplierInterpreterAddition=()->new FitCoreBasicAdditionImpl();
        //FitCoreBasicAdditionImpl fitCoreBasicAdditionImpl=supplierInterpreterAddition.get();
        //Supplier<FitCoreBasicSubtractionImpl> supplierInterpreterSubtraction=()->new FitCoreBasicSubtractionImpl();
        //FitCoreBasicSubtractionImpl fitCoreBasicSubtractionImpl=supplierInterpreterSubtraction.get();
        //Supplier<FitCoreBasicMultiplicationImpl> supplierInterpreterMultiplication=()->new FitCoreBasicMultiplicationImpl();
        //FitCoreBasicMultiplicationImpl fitCoreBasicMultiplicationImpl=supplierInterpreterMultiplication.get();
        //Supplier<FitCoreBasicDivisionImpl> supplierInterpreterDivision=()->new FitCoreBasicDivisionImpl();
        //FitCoreBasicDivisionImpl fitCoreBasicDivisionImpl=supplierInterpreterDivision.get();
        //// 计算11+8的值
        //fitCoreBasicAdditionImpl.interpret(fitCoreBasicContentImpl);
        //// 计算11-8的值
        //fitCoreBasicSubtractionImpl.interpret(fitCoreBasicContentImpl);
        //// 计算11*8的值
        //fitCoreBasicMultiplicationImpl.interpret(fitCoreBasicContentImpl);
        //// 计算11/8的值
        //fitCoreBasicDivisionImpl.interpret(fitCoreBasicContentImpl);
    }

    /**
     * Test my ref.
     */

    public void testMyRef(){
        //Map<Integer,String> map = new HashMap<>();
        //map.put(1, "A");
        //map.put(2, "B");
        //map.put(3, "C");
        //map.put(1, "A");
        //map.put(2, "B");
        //map.put(3, "C");
        //BiConsumer<Integer,String> biConsumer1 = (key,value) ->System.out.println("Key:"+ key+" Value:"+ value);
        //BiConsumer<Integer,String> biConsumer2 = (key,value) ->System.out.println("Value:"+ value+" Key:"+ key);
        //map.forEach(biConsumer1.andThen(biConsumer2));
        //BiFunction<Integer, Integer, String> biFunction1 = (num1,num2) -> "BiFunction:" +(num1 + num2);
        //Function<String, String> biFunction2 = (string) -> "Function:" +(string);
        //System.out.println(biFunction1.apply(50,25));
        //System.out.println(biFunction1.andThen(biFunction2).apply(50,25));
        //BiPredicate<Integer, String> condition = (i,s)-> i>20 && s.startsWith("R");
        //System.out.println(condition.test(10,"Ram"));
        //System.out.println(condition.test(30,"Shyam"));
        //System.out.println(condition.test(30,"Ram"));
        //BiPredicate<Integer, String> condition1 = (i,s)-> i>20 && s.startsWith("T");
        //BiPredicate<Integer, String> condition2 = (i,s)-> i<100 && s.endsWith("y");
        //BiPredicate<Integer, String> condition3 = (i,s)-> i>100 && s.endsWith("y");
        //SOPL.accept(condition1.and(condition2).test(25,"Topicsky"));
        //SOPL.accept(condition2.or(condition3).test(25,"Topicsky"));
        //SOPL.accept(condition3.test(25,"Topicsky"));
        //SOPL.accept(condition3.negate().test(25,"Topicsky"));
        BinaryOperator<Integer> bina = (x,y) ->{int temp = x+y;System.out.println(x+"+"+y+"="+temp);return x+y;};
        System.out.println(bina.apply(2,4));
        Comparator comparator = new Comparator(){
            @Override
            public int compare(Object o1,Object o2){
                boolean b=Integer.parseInt(o1.toString())>Integer.parseInt(o2.toString());
                if(b){
                    return 1;
                }else {
                    return -1;
                }
            }

            @Override
            public boolean equals(Object obj){
                return false;
            }
        };
    }
}
