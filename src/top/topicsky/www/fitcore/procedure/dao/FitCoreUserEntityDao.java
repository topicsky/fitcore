package top.topicsky.www.fitcore.procedure.dao;

import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.FitCoreUserEntity;

import java.io.Serializable;
import java.util.List;

/**
 * The interface Fit core user entity dao.
 */
@Repository(value="fitCoreUserEntityDao")
@Transactional
@CacheNamespace(size=512)
public interface FitCoreUserEntityDao extends Serializable{
    /**
     * Select log in int.
     *
     * @param pojo
     *         the fit core cname
     *
     * @return the int
     */
    FitCoreUserEntity selectLogIn(@Param("pojo") FitCoreUserEntity pojo);

    /**
     * Insert int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insert(@Param("pojo") FitCoreUserEntity pojo);

    /**
     * Insert selective int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insertSelective(@Param("pojo") FitCoreUserEntity pojo);

    /**
     * Insert list int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insertList(@Param("pojos") List<FitCoreUserEntity> pojo);

    /**
     * Update int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int update(@Param("pojo") FitCoreUserEntity pojo);
}
