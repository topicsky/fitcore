package top.topicsky.www.fitcore.procedure.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.topicsky.www.fitcore.global.entity.MycodeEntity;
import top.topicsky.www.fitcore.global.util.inter.plugs.FitCoreInitSpringMVCBeanInter;
import top.topicsky.www.fitcore.procedure.service.MycodeEntityServiceInter;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 05 月 31 日 19 时 11 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
@RequestMapping("/mycode")
public class MyCodeControl implements Serializable{
    /**
     * The Mycode entity configuration.
     */
    @Qualifier(value="mycodeEntity_Constructor_NoParam")
    @Autowired
    MycodeEntity mycodeEntity_Configuration;
    @Qualifier("fitCoreInitSpringMVCBeanImpl")
    @Autowired
    private FitCoreInitSpringMVCBeanInter fitCoreInitSpringMVCBeanImpl;
    @Qualifier("mycodeEntityServiceImpl")
    @Autowired
    private MycodeEntityServiceInter mycodeEntityServiceImpl;
    @Qualifier("mycodeEntity")
    @Autowired
    private MycodeEntity mycodeEntity_Annotation;

    /**
     * Init binder.
     *
     * @param binder
     *         the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder){
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat,true));
    }

    /**
     * Redirect string.
     *
     * @return the string
     */
//访问首页，静态访问
    @RequestMapping(value="/index", method=RequestMethod.POST)
    public String redirect(){
        return "redirect:/model/production/index.jsp";
    }

    /**
     * Query model and view.
     *
     * @param request
     *         the request
     * @param modelAndView
     *         the model and view
     */
    @ModelAttribute
    public void queryModelAndView(HttpServletRequest request,ModelAndView modelAndView){
        /*
        * 这是通过上下文 自定义 name 取得 bean 的实例
        * */
/*        String[] beans={"mycodeEntity"};
        Map<Object,Object> mvcBean=fitCoreInitSpringMVCBeanImpl.InitSpringMVCBean(beans);
        String mycodeid=request.getParameter("myCodeId");
        MycodeEntity myCode_ServletContext=(MycodeEntity)mvcBean.get("mycodeEntity");
        myCode_ServletContext.setMyCodeId(Long.valueOf(mycodeid));
        modelAndView.addObject("myCode_ServletContextMode",myCode_ServletContext);
        *//*
        * 这是通过配置 @Configuration 和 @Bean 取得的实例
        * *//*
        mycodeEntity_Configuration.setMyCodeId(Long.valueOf(mycodeid));
        modelAndView.addObject("myCode_ConfigurationMode",mycodeEntity_Configuration);
        *//*
        * 这是通过配置 @Annotation 取得的实例
        * *//*
        mycodeEntity_Annotation.setMyCodeId(Long.valueOf(mycodeid));
        modelAndView.addObject("myCode_AnnotationMode",mycodeEntity_Annotation);*/
    }

    /**
     * Query string.
     *
     * @param modelAndView
     *         the model and view
     *
     * @return the string
     */
    @RequestMapping(value="/query", method=RequestMethod.GET)
    public String query(ModelAndView modelAndView){
        //MycodeEntity myCode_ServletContext=mycodeEntityServiceImpl.selectById(((MycodeEntity)(modelAndView.getModel().get("myCode_ServletContextMode"))).getMyCodeId());
        //MycodeEntity myCode_Configuration=mycodeEntityServiceImpl.selectById(((MycodeEntity)(modelAndView.getModel().get("myCode_ConfigurationMode"))).getMyCodeId());
        //MycodeEntity myCode_Annotation=mycodeEntityServiceImpl.selectById(((MycodeEntity)(modelAndView.getModel().get("myCode_AnnotationMode"))).getMyCodeId());
        //System.out.println(myCode_ServletContext);
        //System.out.println(myCode_Configuration);
        //System.out.println(myCode_Annotation);
        return "MyCode";
    }

    /**
     * Test rest ful string.
     *
     * @param id
     *         the id
     *
     * @return the string
     */
//启用restful跳转
    @RequestMapping(value="/{vid}/testRestFul", method=RequestMethod.POST)
    public String testRestFul(@PathVariable("vid") String id){
        System.out.println("id="+id);
        return "testRestFul";
    }
}
