package top.topicsky.www.fitcore.procedure.control.predicate;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInBeanDto;
import top.topicsky.www.fitcore.global.resource.constant.FitCoreStaticFinalLayout;
import top.topicsky.www.fitcore.procedure.control.process.FitLogInServletProcessImpl;
import top.topicsky.www.fitcore.procedure.service.predicate.FitLogInServicePredicateImpl;
import top.topicsky.www.fitcore.procedure.service.process.FitLogInServiceProcessImpl;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control.predicate.system.login
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 12 日 10 时 49 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
public class FitLogInServletPredicateImpl implements Serializable{
    /**
     * Get check boolean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     *
     * @return the boolean
     */
    public static boolean getCheck(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        return Arrays.asList(fitCoreLogInBeanDto)
                .stream()
                .allMatch(fitCoreLogInBeanMatch->
                        !(fitCoreLogInBeanMatch.getFitCoreLogInFormDto().getSavetime().equals(""))
                                &&fitCoreLogInBeanMatch.getFitCoreLogInFormDto().getSavetime()==null
                );
    }

    /**
     * Get bean init boolean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     *
     * @return the boolean
     */
    public static boolean getBeanInit(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        return Arrays.asList(fitCoreLogInBeanDto)
                .stream()
                .allMatch(fitCoreLogInBeanMatch->
                        !(fitCoreLogInBeanMatch.getFitCoreLogInFormDto().getUsername().equals(""))
                                &&fitCoreLogInBeanMatch.getFitCoreLogInFormDto().getPassword()!=null
                                &&!(fitCoreLogInBeanMatch.getFitCoreLogInFormDto().getUsername().equals(""))
                                &&fitCoreLogInBeanMatch.getFitCoreLogInFormDto().getPassword()!=null
                );
    }

    /**
     * Get bean dispatcher string.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     *
     * @return the string
     */
    public static String getBeanDispatcher(FitCoreLogInBeanDto fitCoreLogInBeanDto){
        Boolean booleanDispatcher=FitLogInServiceProcessImpl.fitLogInServiceProcessLogin(
                fitCoreLogInBeanDto,
                FitLogInServicePredicateImpl::getProcessLogin);
        Runnable runnableDispatcher=()->FitLogInServletProcessImpl.fitLogInServletProcessService(
                fitCoreLogInBeanDto,
                FitLogInServletPredicateImpl::getCheck);
        try{
            if(booleanDispatcher){
                runnableDispatcher.run();
                return FitCoreStaticFinalLayout.INDEXJSP;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return FitCoreStaticFinalLayout.LOGINJSP;
    }
}
