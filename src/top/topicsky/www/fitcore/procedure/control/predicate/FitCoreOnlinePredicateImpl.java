package top.topicsky.www.fitcore.procedure.control.predicate;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import top.topicsky.www.fitcore.global.dto.FitCoreOnlineFormDto;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control.predicate
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 19 日 11 时 20 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
public class FitCoreOnlinePredicateImpl implements Serializable{
    /**
     * Fit core online predicate init string.
     *
     * @param initString
     *         the init string
     *
     * @return the string
     */
    public static String FitCoreOnlinePredicateInit(String initString){
        /**
         * 转发初始化处理过程
         * */
        return initString;
    }

    /**
     * Fit core online process init nodel boolean.
     *
     * @param model
     *         the model
     *
     * @return the boolean
     */
    public static Boolean FitCoreOnlineProcessInitNodel(Model model){
        /**
         * 页面元素初始化处理过程
         * */
        model.addAttribute("fitCoreOnlineFormDto",new FitCoreOnlineFormDto());
        return true;
    }

    /**
     * Fit core online predicate init process boolean.
     *
     * @param fitCoreOnlineFormDto
     *         the fit core online form dto
     *
     * @return the boolean
     */
    public static Boolean FitCoreOnlinePredicateInitProcess(FitCoreOnlineFormDto fitCoreOnlineFormDto){
        return Arrays.asList(fitCoreOnlineFormDto)
                .stream()
                .allMatch(fitCoreLogInBeanMatch->
                        !(fitCoreOnlineFormDto.getUsername().equals(""))
                                &&fitCoreOnlineFormDto.getPassword()!=null
                                &&!(fitCoreOnlineFormDto.getUsername().equals(""))
                                &&fitCoreOnlineFormDto.getPassword()!=null
                );
    }
}
