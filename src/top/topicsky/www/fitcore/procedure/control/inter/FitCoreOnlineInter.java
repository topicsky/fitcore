package top.topicsky.www.fitcore.procedure.control.inter;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import top.topicsky.www.fitcore.global.dto.FitCoreOnlineFormDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control.inter
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 19 日 11 时 19 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
public interface FitCoreOnlineInter extends Serializable{
    /**
     * Online init string.
     *
     * @param Init
     *         the init
     * @param model
     *         the model
     *
     * @return the string
     */
    public String onlineInit(String Init,Model model);

    /**
     * Online process string.
     *
     * @param fitCoreOnlineFormDto
     *         the fit core online form dto
     * @param Process
     *         the process
     * @param model
     *         the model
     * @param httpServletRequest
     *         the http servlet request
     * @param httpServletResponse
     *         the http servlet response
     *
     * @return the string
     */
    public String onlineProcess(FitCoreOnlineFormDto fitCoreOnlineFormDto,
                                String Process,
                                Model model,
                                HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse);
}
