package top.topicsky.www.fitcore.procedure.control.core;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.core.FitCoreAdapterImpl;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 15 日 09 时 43 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 *
 * @param <SI>
 *         the type parameter
 * @param <RT>
 *         the type parameter
 */
@Controller
@Transactional
public abstract class FitCoreControlImpl<SI,RT> extends FitCoreAdapterImpl implements FitCoreControlInter, Serializable{
    @Override
    public Object fitCoreExceptionCoreAdapterInit(Object o){
        return null;
    }

    @Override
    public Object fitCoreExceptionCoreBaseInit(Object o){
        return null;
    }

    @Override
    public Object fitCoreExceptionCoreInit(Object o){
        return null;
    }

    @Override
    public Object fitCoreControlInit(Object o){
        return o;
    }

    @Override
    public Object fitCoreInit(Object o){
        return o;
    }

    @Override
    public Object fitCoreBaseInit(Object o){
        return super.fitCoreBaseInit(o);
    }

    @Override
    public Object fitCoreAdapterInit(Object o){
        return super.fitCoreAdapterInit(o);
    }
}
