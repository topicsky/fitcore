package top.topicsky.www.fitcore.procedure.service.process;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInBeanDto;
import top.topicsky.www.fitcore.global.resource.constant.FitCoreStaticFinalVariable;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.service.process.system.login
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 12 日 17 时 40 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Service
@Transactional
public class FitLogInServiceProcessImpl implements Serializable{
    /**
     * Fit log in service process login boolean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     * @param predicateServiceLogin
     *         the predicate service login
     *
     * @return the boolean
     */
    public static Boolean fitLogInServiceProcessLogin(FitCoreLogInBeanDto fitCoreLogInBeanDto,Engine_Basic_Object_Inter<FitCoreLogInBeanDto,Boolean> predicateServiceLogin){
        return predicateServiceLogin.fitCoreEngine(fitCoreLogInBeanDto);
    }

    /**
     * Fit log in service process boolean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     * @param predicateServiceCheckProcess
     *         the predicate service check process
     *
     * @return the boolean
     */
    public static Boolean fitLogInServiceProcess(FitCoreLogInBeanDto fitCoreLogInBeanDto,Engine_Basic_Object_Inter<FitCoreLogInBeanDto,Boolean> predicateServiceCheckProcess){
        if(predicateServiceCheckProcess.fitCoreEngine(fitCoreLogInBeanDto)){
            return FitCoreStaticFinalVariable.FITTRUE;
        }else{
            return FitCoreStaticFinalVariable.FITFALSE;
        }
    }

    /**
     * Fit core log in form check boolean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     * @param predicateServiceCheckForm
     *         the predicate service check form
     *
     * @return the boolean
     */
    public static Boolean fitCoreLogInFormCheck(FitCoreLogInBeanDto fitCoreLogInBeanDto,Engine_Basic_Object_Inter<FitCoreLogInBeanDto,Boolean> predicateServiceCheckForm){
        return predicateServiceCheckForm.fitCoreEngine(fitCoreLogInBeanDto);
    }

    /**
     * Fit core user entity check boolean.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean
     * @param predicateServiceCheckEntity
     *         the predicate service check entity
     *
     * @return the boolean
     */
    public static Boolean fitCoreUserEntityCheck(FitCoreLogInBeanDto fitCoreLogInBeanDto,Engine_Basic_Object_Inter<FitCoreLogInBeanDto,Boolean> predicateServiceCheckEntity){
        return predicateServiceCheckEntity.fitCoreEngine(fitCoreLogInBeanDto);
    }
}
