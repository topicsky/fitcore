package top.topicsky.www.fitcore.procedure.service.process;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.dto.FitCoreLogInBeanDto;
import top.topicsky.www.fitcore.global.entity.FitCoreUserEntity;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import java.io.Serializable;
import java.util.Map;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.service.process.system.login
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 12 日 17 时 40 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Service
@Transactional
public class FitCoreUtilServiceProcessImpl implements Serializable{
    /**
     * Generate excel view map.
     *
     * @param fitCoreLogInBeanDto
     *         the fit core log in bean dto
     * @param predicateExcelView
     *         the predicate excel view
     *
     * @return the map
     */
    public static Map<String,String> generateExcelView(FitCoreLogInBeanDto fitCoreLogInBeanDto,
                                                       Engine_Basic_Object_Inter<FitCoreLogInBeanDto,Map<String,String>> predicateExcelView){
        return predicateExcelView.fitCoreEngine(fitCoreLogInBeanDto);
    }

    /**
     * Generate excel view source map.
     *
     * @param fitCoreUserEntity
     *         the fit core user entity
     * @param predicateExcelViewSource
     *         the predicate excel view source
     *
     * @return the map
     */
    public static Map<String,String> generateExcelViewSource(FitCoreUserEntity fitCoreUserEntity,
                                                             Engine_Basic_Object_Inter<FitCoreUserEntity,Map<String,String>> predicateExcelViewSource){
        return predicateExcelViewSource.fitCoreEngine(fitCoreUserEntity);
    }
}
