package top.topicsky.www.fitcore.procedure.service.inter;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.FitCoreLog4jEntity;

import java.io.Serializable;
import java.util.List;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.service.inter.logs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 07 日 17 时 45 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Service
@Transactional
public interface FitCoreLog4jServiceInter extends Serializable{
    /**
     * 根据Id查询记录
     *
     * @param fitCore_stamp
     *         the fit core stamp
     *
     * @return the fit core log 4 j entity
     */
    FitCoreLog4jEntity selectById(@Param("fitCore_stamp") Long fitCore_stamp);

    /**
     * 根据给定的记录查询一批记录
     *
     * @param fitCore_stamps
     *         the fit core stamps
     *
     * @return the list
     */
    List<FitCoreLog4jEntity> selectByIds(@Param("fitCore_stamps") List<Long> fitCore_stamps);

    /**
     * 新增记录
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insert(@Param("pojo") FitCoreLog4jEntity pojo);

    /**
     * 新增记录，只匹配有值的字段
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insertSelective(@Param("pojo") FitCoreLog4jEntity pojo);

    /**
     * 根据Id更新记录
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int updateById(@Param("pojo") FitCoreLog4jEntity pojo);

    /**
     * 根据Id更新记录,只更新有值的字段
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int updateSelectiveById(@Param("pojo") FitCoreLog4jEntity pojo);

    /**
     * 根据Id删除记录
     *
     * @param fitCore_stamp
     *         the fit core stamp
     *
     * @return the int
     */
    int deleteById(@Param("fitCore_stamp") Long fitCore_stamp);

    /**
     * 根据Id删除一批记录
     *
     * @param fitCore_stamps
     *         the fit core stamps
     *
     * @return the int
     */
    int deleteByIds(@Param("fitCore_stamps") List<Long> fitCore_stamps);
}
