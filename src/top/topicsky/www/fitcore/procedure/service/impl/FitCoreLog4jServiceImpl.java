package top.topicsky.www.fitcore.procedure.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.FitCoreLog4jEntity;
import top.topicsky.www.fitcore.global.resource.constant.FitCoreStaticFinalNumber;
import top.topicsky.www.fitcore.procedure.service.inter.FitCoreLog4jServiceInter;

import java.io.Serializable;
import java.util.List;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.service.impl.logs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 07 日 17 时 46 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Service
@Transactional
public class FitCoreLog4jServiceImpl implements FitCoreLog4jServiceInter, Serializable{
    @Override
    public FitCoreLog4jEntity selectById(Long fitCore_stamp){
        return null;
    }

    @Override
    public List<FitCoreLog4jEntity> selectByIds(List<Long> fitCore_stamps){
        return null;
    }

    @Override
    public int insert(FitCoreLog4jEntity pojo){
        return FitCoreStaticFinalNumber.FITZERO;
    }

    @Override
    public int insertSelective(FitCoreLog4jEntity pojo){
        return FitCoreStaticFinalNumber.FITZERO;
    }

    @Override
    public int updateById(FitCoreLog4jEntity pojo){
        return FitCoreStaticFinalNumber.FITZERO;
    }

    @Override
    public int updateSelectiveById(FitCoreLog4jEntity pojo){
        return FitCoreStaticFinalNumber.FITZERO;
    }

    @Override
    public int deleteById(Long fitCore_stamp){
        return FitCoreStaticFinalNumber.FITZERO;
    }

    @Override
    public int deleteByIds(List<Long> fitCore_stamps){
        return FitCoreStaticFinalNumber.FITZERO;
    }
}
