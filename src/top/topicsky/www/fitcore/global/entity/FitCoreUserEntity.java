package top.topicsky.www.fitcore.global.entity;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.entity
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 06 日 15 时 11 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */

/**
 * 使用 @Data 相当于同时使用了@ToString、@EqualsAndHashCode、@Getter、@Setter和@RequiredArgsConstructor这些注解
 *
 * @Cleanup 他可以帮我们在需要释放的资源位置自动加上释放代码
 * @Synchronized 可以帮我们在方法上添加同步代码块
 * @XmlRootElement 定义了注解注入实例
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@ToString(callSuper=true)
/**
 * 这里如果不写false,会报错,
 * Error:(21, 1) 警告: Generating equals/hashCode implementation but without a call to superclass,
 * even though this class does not extend java.lang.Object. If this is intentional,
 * add '@EqualsAndHashCode(callSuper=false)' to your type.
 */
@EqualsAndHashCode(callSuper=false)
@RequiredArgsConstructor
@AllArgsConstructor
@Slf4j
@Repository
@Transactional
@XmlRootElement
public class FitCoreUserEntity implements Serializable{
    private static final long serialVersionUID=7541576884075637341L;
    /*
    * 系统内编号
    * */
    private Long fitCoreId;
    /*
     * 特殊业务编号
     * */
    private String fitCoreUuid;
    /*
    * 头像
    * */
    private String fitCoreImage;
    /*
    * 主题
    * */
    private String fitCoreTheme;
    /*
    * 中文名
    * */
    private String fitCoreCname;
    /*
    * 英文名
    * */
    private String fitCoreEname;
    /*
    * 昵称
    * */
    private String fitCoreDisplayname;
    /*
    * 年龄
    * */
    private String fitCoreAge;
    /*
    *生日
    * */
    private String fitCoreBirthday;
    /*
    *电子邮件地址
    * */
    private String fitCoreEmail;
    /*
    * 密码
    * */
    private String fitCorePassword;
    /*
    * 移动电话
    * */
    private String fitCorePhone;
    /*
    * 固定电话
    * */
    private String fitCoreTelphone;
    /*
    * 国家
    * */
    private String fitCoreCountry;
    /*
    * 省
    * */
    private String fitCoreProvince;
    /*
    * 城市
    * */
    private String fitCoreCity;
    /*
    * 街道
    * */
    private String fitCoreStreet;
    /*
    * 公司
    * */
    private String fitCoreCompany;
    /*
    * 部门
    * */
    private String fitCoreDepartment;
    /*
    * IP地址
    * */
    private String fitCoreIp;
}
