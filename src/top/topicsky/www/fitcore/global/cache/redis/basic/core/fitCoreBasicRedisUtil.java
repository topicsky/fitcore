package top.topicsky.www.fitcore.global.cache.redis.basic.core;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * 所在项目名称 ： fitcorebate
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.cache.redis.basic.core
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 20 日 18 时 24 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class fitCoreBasicRedisUtil{
    /**
     * Redis服务器IP
     */
    private static String ADDR="192.168.196.100";
    /**
     * Redis的端口号,默认
     */
    private static int PORT=6379;
    /**
     * 访问密码
     */
    private static String AUTH="root";
    /**
     * 可用连接实例的最大数目，默认值为8
     * 如果赋值为-1，则表示不限制
     * 如果pool已经分配了maxActive个jedis实例
     * 则此时pool的状态为exhausted(耗尽)
     */
    private static int MAX_ACTIVE=1024;
    /**
     * 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8
     */
    private static int MAX_IDLE=200;
    /**
     * 等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时
     * 如果超过等待时间，则直接抛出JedisConnectionException
     */
    private static int MAX_WAIT=10000;
    /**
     * 超时时间
     */
    private static int TIMEOUT=10000;
    /**
     * 在borrow一个jedis实例时，是否提前进行校验操作
     * 如果为true，则得到的jedis实例均是可用的
     */
    private static boolean TEST_ON_BORROW=true;
    /*
    * redis 实例
    * */
    private static JedisPool jedisPool=null;

    /**
     * 初始化Redis连接池
     */
    static{
        try{
            JedisPoolConfig config=new JedisPoolConfig();
            config.setMaxIdle(MAX_IDLE);
            config.setMaxWaitMillis(MAX_WAIT);
            config.setTestOnBorrow(TEST_ON_BORROW);
            jedisPool=new JedisPool(config,ADDR,PORT,TIMEOUT,AUTH);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 获取Jedis实例
     *
     * @return
     */
    public synchronized static Jedis getJedis(){
        try{
            if(jedisPool!=null){
                Jedis resource=jedisPool.getResource();
                return resource;
            }else{
                return null;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 释放jedis资源
     *
     * @param jedis
     */
    public static void returnResource(final Jedis jedis){
        if(jedis!=null){
            jedisPool.returnResource(jedis);
        }
    }
}
