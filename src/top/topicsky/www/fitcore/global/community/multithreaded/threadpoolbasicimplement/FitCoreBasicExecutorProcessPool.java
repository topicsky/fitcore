package top.topicsky.www.fitcore.global.community.multithreaded.threadpoolbasicimplement;
/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.threadpoolimplement
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 06 日 10 时 54 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 线程处理类
 */
public class FitCoreBasicExecutorProcessPool{
    /*这里是内部构造实例*/
    private static FitCoreBasicExecutorProcessPool pool=null;
    /*默认最大线程池上限*/
    private static Integer threadMax=10;
    /*Executor线程池*/
    private ExecutorService executor;

    private FitCoreBasicExecutorProcessPool(){
        SOPL.accept("最大线程数："+threadMax);
        executor=FitCoreBasicExecutorServiceFactory.getInstance().createFixedThreadPool(threadMax);
    }

    /**
     * Get instance fit core basic executor process pool.
     *
     * @return the fit core basic executor process pool
     */
    public static FitCoreBasicExecutorProcessPool getInstance(){
        return pool=new FitCoreBasicExecutorProcessPool();
    }

    /**
     * Get instance fit core basic executor process pool.
     *
     * @param threadMaxCustom
     *         the thread max custom
     *
     * @return the fit core basic executor process pool
     */
    public static FitCoreBasicExecutorProcessPool getInstance(Integer threadMaxCustom){
        threadMax=threadMaxCustom;
        return pool=new FitCoreBasicExecutorProcessPool();
    }

    /**
     * 关闭线程池，这里要说明的是：调用关闭线程池方法后，线程池会执行完队列中的所有任务才退出
     */
    public void shutdown(){
        executor.shutdown();
    }

    /**
     * 提交任务到线程池 Runnable ，可以接收线程返回值
     *
     * @param task
     *         the task
     *
     * @return the future
     */
    public Future<?> submit(Runnable task){
        return executor.submit(task);
    }

    /**
     * 提交任务到线程池 Callable ，可以接收线程返回值
     *
     * @param task
     *         the task
     *
     * @return the future
     */
    public Future<?> submit(Callable<?> task){
        return executor.submit(task);
    }

    /**
     * 直接提交任务到线程池，无返回值
     *
     * @param task
     *         the task
     */
    public void execute(Runnable task){
        executor.execute(task);
    }
}
