package top.topicsky.www.fitcore.global.community.multithreaded.threadpoolintermediateimplement;


import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.global.community.multithreaded.threadpoolintermediateimplement.FitCoreInterMediateThreadPoolImpl.taskList;
import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.threadpoolimplement
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 17 时 05 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreInterMediateThreadBeanImpl implements FitCoreInterMediateThreadBeanInter, Serializable{
    private static FitCoreInterMediateThreadPoolImpl instance=FitCoreInterMediateThreadPoolImpl.getInstance();

    /**
     * Main.
     *
     * @param args
     *         the args
     *
     * @throws InterruptedException
     *         the interrupted exception
     */
    public static void main(String[] args) throws InterruptedException{
        Integer temp=0;
        for(int i=0;i<20;i++){
            instance.addTask(new FitCoreInterMediateThreadTaskBeanImpl());
        }
        while(true){
            Thread.sleep(1000);
            SOPL.accept(instance.getInfo());
            for(FitCoreInterMediateThreadTaskImpl gg : taskList){
                SOPL.accept(gg.getTaskId()+"--"+gg.getSubmitTime());
            }
        }
        //instance.destroy();
    }

    /**
     * Invock init topicsky.
     */

    public void invockInitTopicsky(){
        /*测试框架*/
        this.initTopicsky();
    }

    @Override
    public void initTopicsky(){
        SOPL.accept(instance.getInfo());
    }
}
