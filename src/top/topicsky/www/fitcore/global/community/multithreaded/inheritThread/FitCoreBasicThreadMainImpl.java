package top.topicsky.www.fitcore.global.community.multithreaded.inheritThread;


import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.function.Supplier;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.inheritThread
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 09 时 58 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicThreadMainImpl implements FitCoreBasicThreadMainInter, Serializable{
    /**
     * Main.
     *
     * @param args
     *         the args
     */
    public static void main(String[] args){
        Supplier<FitCoreBasicThreadInheritImpl> supplierT=()->new FitCoreBasicThreadInheritImpl();
        supplierT.get().start();
        supplierT.get().start();
    }

    /**
     * Invock init topicsky.
     */

    public void invockInitTopicsky(){
        /*测试框架*/
        this.initTopicsky();
    }

    @Override
    public void initTopicsky(){
    }
}
