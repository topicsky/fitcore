package top.topicsky.www.fitcore.global.community.multithreaded.implementexecutor;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.Callable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.implementexecutor
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 09 时 54 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicExecutorImpl implements Callable<Object>, Serializable{
    private String taskNum;

    /**
     * Instantiates a new Fit core basic executor.
     *
     * @param taskNum
     *         the task num
     */
    FitCoreBasicExecutorImpl(String taskNum){
        this.taskNum=taskNum;
    }

    @Override
    public Object call() throws Exception{
        SOPL.accept(">>>"+taskNum+"任务启动");
        Date dateTmp1=new Date();
        Thread.sleep(1000);
        Date dateTmp2=new Date();
        long time=dateTmp2.getTime()-dateTmp1.getTime();
        SOPL.accept(">>>"+taskNum+"任务终止");
        return taskNum+"任务返回运行结果,任务时间【"+time+"毫秒】";
    }
}

