package top.topicsky.www.fitcore.global.community.hibernatevalidator;


import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.hibernatevalidator
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 04 日 11 时 36 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreHibernateValidatorBeanImpl implements FitCoreHibernateValidatorBeanInter, Serializable{
    /**
     * Invock init topicsky.
     */

    public void invockInitTopicsky(){
        /*测试框架*/
        String typeString=null;
        Character typeCharacter="A".charAt(0);
        Integer typeInteger=30;
        Double typeDouble=20.02D;
        Float typeFloat=10.1F;
        Boolean typeBoolean=true;
        Collection typeCollection=new ArrayList();
        String[] typeStringArray=new String[2];
        Supplier<FitCoreHibernateValidatorDto>
                supplierFitCoreHibernateValidatorDto=
                ()->new FitCoreHibernateValidatorDto(
                        typeString,
                        typeCharacter,
                        typeInteger,
                        typeDouble,
                        typeFloat,
                        typeBoolean,
                        typeCollection,
                        typeStringArray);
        this.initTopicsky(new FitCoreHibernateValidatorDto(
                typeString,
                typeCharacter,
                typeInteger,
                typeDouble,
                typeFloat,
                typeBoolean,
                typeCollection,
                typeStringArray));
    }

    @Override
    public void initTopicsky(@Valid FitCoreHibernateValidatorDto fitCoreHibernateValidatorDto){
        SOPL.accept(fitCoreHibernateValidatorDto.getTypeString());
        SOPL.accept(fitCoreHibernateValidatorDto.getTypeCharacter());
        SOPL.accept(fitCoreHibernateValidatorDto.getTypeInteger());
        SOPL.accept(fitCoreHibernateValidatorDto.getTypeDouble());
        SOPL.accept(fitCoreHibernateValidatorDto.getTypeFloat());
        SOPL.accept(fitCoreHibernateValidatorDto.getTypeBoolean());
        SOPL.accept(fitCoreHibernateValidatorDto.getTypeCollection().size());
        SOPL.accept(fitCoreHibernateValidatorDto.getTypeStringArray().length);
    }
}
