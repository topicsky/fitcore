package top.topicsky.www.fitcore.global.community.functionalinterfaces.topicskyrunnable;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： topicskycode
 * 操作文件所在包路径　： languagefeature.functionalinterfaces.topicskyrunnable
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 03 日 10 时 09 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class BasicMainImpl implements BasicMainInter, Serializable{
    /**
     * Invock init topicsky.
     */

    public void invockInitTopicsky(){
        /*测试框架*/
        this.initTopicsky();
    }

    @Override
    public void initTopicsky(){
        SOPL.accept(
                /*首先调用 Process 过程，将 Runnable 接口参数函数化，传入 BasicRunnableImpl 的 initRunnable 实现过程*/
                BasicProcessImpl.initProcess(
                        /*接口函数需要的实现过程函数*/
                        BasicRunnableImpl::initRunnable)
        );
    }
}
