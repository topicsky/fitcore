package top.topicsky.www.fitcore.global.community.functionalinterfaces.topicskycallable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

import static top.topicsky.www.fitcore.system.kernel.common.custom.CustomUtil.SOPF;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.functionalinterfaces.topicskycallable
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 04 日 09 时 43 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class BasicMainImpl implements Callable<Integer>, Serializable{
    private int number;

    /**
     * Instantiates a new Basic main.
     *
     * @param number
     *         the number
     */
    public BasicMainImpl(int number){
        super();
        this.number=number;
    }

    /**
     * Main.
     *
     * @param args
     *         the args
     */
    public static void main(String[] args){
        /*建立线程池，初始化线程池数量*/
        ThreadPoolExecutor execute=(ThreadPoolExecutor)Executors.newFixedThreadPool(3);
        /*建立返回值结果集存储对象*/
        List<Future<Integer>> results=new ArrayList<Future<Integer>>();
        Random random=new Random();
        /*建立任务数量及代号*/
        for(int i=0;i<10;i++){
            int number=i;
            BasicMainImpl calcu=new BasicMainImpl(number);
            Future<Integer> result=execute.submit(calcu);
            results.add(result);
        }
        do{
            SOPF.initCustom("主信息:已完成任务数量:%d\n",execute.getCompletedTaskCount());
            for(int i=0;i<results.size();i++){
                Future<Integer> result=results.get(i);
                SOPF.initCustom("是否已完成:%s\n",result.isDone());
            }
            try{
                TimeUnit.MILLISECONDS.sleep(50);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }while(execute.getCompletedTaskCount()<results.size());
        for(Future<Integer> fus : results){
            try{
                //SOPF.initCustom("主信息: 任务是否已完成 %d: %s\n",result.isDone());
                System.out.printf("已完成的任务编号: %d\n",fus.get());
            }catch(InterruptedException e){
                e.printStackTrace();
            }catch(ExecutionException e){
                e.printStackTrace();
            }
        }
        execute.shutdown();
    }

    @Override
    public Integer call() throws Exception{
        int result=1;
        for(int i=0;i<=number+1;i++){
            result=i;
            TimeUnit.MILLISECONDS.sleep(20);
        }
        SOPF.initCustom("线程名称:%s:",Thread.currentThread().getName());
        SOPF.initCustom("执行的任务编码:%d\n",result);
        return result;
    }
}
