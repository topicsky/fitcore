package top.topicsky.www.fitcore.global.community.locksprinciple.synchronizedmethod;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.locksprinciple.synchronizedmethod
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 13 时 29 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicSynchronizedMethodImpl implements FitCoreBasicSynchronizedMethodInter, Serializable{
    /*这里是 synchronized 修饰方法 这里 和 this 修饰表示对象一致*/
    @Override
    public synchronized void initMethod(){
        SOPL.accept("这里是 initMethod 方法，既是对象锁，也是方法锁");
        int i=5;
        while(i-->0){
            SOPL.accept(Thread.currentThread().getName()+" : "+i);
            try{
                Thread.sleep(600);
            }catch(InterruptedException ie){
                SOPL.accept(ie.getMessage());
            }
        }
    }

    @Override
    public synchronized void haltMethod(){
        SOPL.accept("这里是 haltMethod 方法，既是对象锁，也是方法锁");
        int i=5;
        while(i-->0){
            SOPL.accept(Thread.currentThread().getName()+" : "+i);
            try{
                Thread.sleep(300);
            }catch(InterruptedException ie){
                SOPL.accept(ie.getMessage());
            }
        }
    }
}
