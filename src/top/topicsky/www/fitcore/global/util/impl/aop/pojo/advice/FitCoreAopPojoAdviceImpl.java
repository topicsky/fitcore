package top.topicsky.www.fitcore.global.util.impl.aop.pojo.advice;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.aop.pojo.advice.FitCoreAopPojoAdviceInter;
import top.topicsky.www.fitcore.global.util.predicate.aop.pojo.advice.FitCoreAopPojoAdvicePredicateImpl;
import top.topicsky.www.fitcore.global.util.process.aop.pojo.advice.FitCoreAopPojoAdviceProcessImpl;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.aop.pojo.advice
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 08 日 14 时 56 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreAopPojoAdviceImpl implements MethodBeforeAdvice, AfterReturningAdvice, MethodInterceptor, FitCoreAopPojoAdviceInter, Serializable{
    @Override
    public void initBefore(String args1,Integer args2) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopPojoAdviceProcessImpl.FitCoreAopPojoAdviceInit("Init Message",
                        FitCoreAopPojoAdvicePredicateImpl::FitCoreAopPojoAdviceInit)
        );
        runnable.run();
        System.out.println("args1:"+args1+",args2:"+args2+"Before");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public Object initAround(String args1,Integer args2) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopPojoAdviceProcessImpl.FitCoreAopPojoAdviceInit("Init Message",
                        FitCoreAopPojoAdvicePredicateImpl::FitCoreAopPojoAdviceInit)
        );
        runnable.run();
        System.out.println("前面拦截....");
        System.out.println("args1:"+args1+",args2:"+args2+"Around");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
        System.out.println("后面拦截.....");
        return null;
    }

    @Override
    public void initAfter(String args1,Integer args2) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopPojoAdviceProcessImpl.FitCoreAopPojoAdviceInit("Init Message",
                        FitCoreAopPojoAdvicePredicateImpl::FitCoreAopPojoAdviceInit)
        );
        runnable.run();
        System.out.println("args1:"+args1+",args2:"+args2+"After");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void initAfterThrowing(String args1,Integer args2,Throwable ex) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopPojoAdviceProcessImpl.FitCoreAopPojoAdviceInit("Init Message",
                        FitCoreAopPojoAdvicePredicateImpl::FitCoreAopPojoAdviceInit)
        );
        runnable.run();
        System.out.println("args1:"+args1+",args2:"+args2+"AfterThrowing");
        System.out.println("异常信息："+ex);
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void initAfterReturning(String args1,Integer args2) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopPojoAdviceProcessImpl.FitCoreAopPojoAdviceInit("Init Message",
                        FitCoreAopPojoAdvicePredicateImpl::FitCoreAopPojoAdviceInit)
        );
        runnable.run();
        System.out.println("args1:"+args1+",args2:"+args2+"AfterReturning");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void afterReturning(Object o,Method method,Object[] objects,Object o1) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopPojoAdviceProcessImpl.FitCoreAopPojoAdviceInit("Init Message",
                        FitCoreAopPojoAdvicePredicateImpl::FitCoreAopPojoAdviceInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public void before(Method method,Object[] objects,Object o) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopPojoAdviceProcessImpl.FitCoreAopPojoAdviceInit("Init Message",
                        FitCoreAopPojoAdvicePredicateImpl::FitCoreAopPojoAdviceInit)
        );
        runnable.run();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable{
        Runnable runnable=()->System.out.println(
                FitCoreAopPojoAdviceProcessImpl.FitCoreAopPojoAdviceInit("Init Message",
                        FitCoreAopPojoAdvicePredicateImpl::FitCoreAopPojoAdviceInit)
        );
        runnable.run();
        System.out.println("前面拦截....");
        Object resObj=methodInvocation.proceed();
        System.out.println("后面拦截.....");
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
        return resObj;
    }
}
