package top.topicsky.www.fitcore.global.util.impl.customformtags.refixform;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import top.topicsky.www.fitcore.global.util.inter.customformtags.refixform.FitCoreFormInter;
import top.topicsky.www.fitcore.global.util.predicate.customformtags.refixform.FitCoreFormPredicateImpl;
import top.topicsky.www.fitcore.global.util.process.customformtags.refixform.FitCoreFormProcessImpl;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.customformtags.refixform
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 17 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreFormImpl implements FitCoreFormInter, Serializable{
    public <OBJ> String FitCoreFormPack(
            Model model,
            OBJ obj,
            String objName,
            String pageForword){
        FitCoreFormProcessImpl.FitCoreFormProcessPack(model,
                obj,
                objName,
                FitCoreFormPredicateImpl::FitCoreFormPredicatePack);
        return pageForword;
    }
}
