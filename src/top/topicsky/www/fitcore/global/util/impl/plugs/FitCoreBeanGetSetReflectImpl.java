package top.topicsky.www.fitcore.global.util.impl.plugs;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.plugs.FitCoreBeanGetSetReflectInter;
import top.topicsky.www.fitcore.global.util.predicate.plugs.FitCoreBeanGetSetReflectPredicateImpl;
import top.topicsky.www.fitcore.global.util.process.plugs.FitCoreBeanGetSetReflectProcessImpl;

import java.io.Serializable;
import java.util.List;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.plugs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 16 时 29 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBeanGetSetReflectImpl implements FitCoreBeanGetSetReflectInter, Serializable{
    public <TI,TR> TR getProperty(List<TI> beanObject){
        //beanObject.add(beanObj);
        //beanObject.add(property);
        return FitCoreBeanGetSetReflectProcessImpl.getPropertyProcess(beanObject,
                FitCoreBeanGetSetReflectPredicateImpl::getPropertyPredicate);
    }

    public <TI,TR> TR setProperty(List<TI> beanObject){
        //beanObject.add(beanObj);
        //beanObject.add(property);
        //beanObject.add(value);
        return FitCoreBeanGetSetReflectProcessImpl.setPropertyProcess(beanObject,
                FitCoreBeanGetSetReflectPredicateImpl::setPropertyPredicate);
    }
}
