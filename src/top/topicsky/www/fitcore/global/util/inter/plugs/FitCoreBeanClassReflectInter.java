package top.topicsky.www.fitcore.global.util.inter.plugs;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.inter.plugs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 08 日 11 时 19 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public interface FitCoreBeanClassReflectInter extends Serializable{
    /**
     * 根据属性名获取属性值
     *
     * @param fieldName
     *         the field name
     * @param object
     *         the object
     *
     * @return the field value by name
     */
    Object getFieldValueByName(String fieldName,Object object);

    /**
     * 获取属性名数组
     *
     * @param object
     *         the object
     *
     * @return the string [ ]
     */
    String[] getFiledName(Object object);

    /**
     * 获取属性类型(type)，属性名(name)，属性值(value)的map组成的list
     *
     * @param object
     *         the object
     *
     * @return the fileds info
     */
    List getFiledsInfo(Object object);

    /**
     * 获取对象的所有属性值，返回一个对象数组
     *
     * @param object
     *         the object
     *
     * @return the object [ ]
     */
    Object[] getFiledValues(Object object);

    /**
     * 调用对象的setter方法，将非基本数据类型统一设置为null值
     *
     * @param object
     *         the object
     *
     * @author linshutao
     */
    void setFieldToNull(Object object);

    /**
     * 调用对象的Set类型的getter方法，可用于实例化集合，避免hibernate的lazy引起的异常
     *
     * @param object
     *         the object
     *
     * @author linshutao
     */
    void callGetMethodForSetField(Object object);
}
