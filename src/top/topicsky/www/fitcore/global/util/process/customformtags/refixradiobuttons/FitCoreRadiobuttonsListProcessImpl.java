package top.topicsky.www.fitcore.global.util.process.customformtags.refixradiobuttons;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import top.topicsky.www.fitcore.global.resource.constant.FitCoreStaticFinalVariable;
import top.topicsky.www.fitcore.system.kernel.engine.Engine_Basic_Object_Inter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.process.customformtags.refixradiobuttons
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 21 日 17 时 08 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreRadiobuttonsListProcessImpl implements Serializable{
    /**
     * Fit core radiobuttons list process pack.
     *
     * @param <OBJ>
     *         the type parameter
     * @param <TX>
     *         the type parameter
     * @param <TY>
     *         the type parameter
     * @param <TZ>
     *         the type parameter
     * @param <TQ>
     *         the type parameter
     * @param <TP>
     *         the type parameter
     * @param <TS>
     *         the type parameter
     * @param <TF>
     *         the type parameter
     * @param <TR>
     *         the type parameter
     * @param model
     *         the model
     * @param obj
     *         the obj
     * @param objProperty
     *         the obj property
     * @param objValue
     *         the obj value
     * @param choiceName
     *         the choice name
     * @param buttonPredicate
     *         the button predicate
     */
    public static <OBJ,TX,TY,TZ,TQ,TP,TS,TF,TR> void FitCoreRadiobuttonsListProcessPack(TX model,
                                                                                        OBJ obj,
                                                                                        TZ objProperty,
                                                                                        TQ objValue,
                                                                                        TP choiceName,
                                                                                        Engine_Basic_Object_Inter<List<TY>,TR> buttonPredicate){
        List<TY> beanObject=new ArrayList<>();
        beanObject.add((TY)obj);
        beanObject.add((TY)objProperty);
        beanObject.add((TY)objValue);
         /*待选择的必须不能为空，需要判断*/
        if(FitCoreStaticFinalVariable.SUCCESS.equals(buttonPredicate.fitCoreEngine(beanObject))){
            ((Model)model).addAttribute(String.valueOf(choiceName),obj);
            System.out.println(FitCoreStaticFinalVariable.SUCCESS);
        }
    }

    /**
     * Fit core radiobuttons list process content pack.
     *
     * @param <OBJ>
     *         the type parameter
     * @param <TX>
     *         the type parameter
     * @param <TY>
     *         the type parameter
     * @param <TZ>
     *         the type parameter
     * @param <TQ>
     *         the type parameter
     * @param <TP>
     *         the type parameter
     * @param <TS>
     *         the type parameter
     * @param <TF>
     *         the type parameter
     * @param <TR>
     *         the type parameter
     * @param model
     *         the model
     * @param listContent
     *         the list content
     * @param contentName
     *         the content name
     * @param contentPredicate
     *         the content predicate
     */
    public static <OBJ,TX,TY,TZ,TQ,TP,TS,TF,TR> void FitCoreRadiobuttonsListProcessContentPack(TX model,
                                                                                               List<TY> listContent,
                                                                                               TS contentName,
                                                                                               Engine_Basic_Object_Inter<List<TY>,TR> contentPredicate){
         /*待选择的必须不能为空，需要判断*/
        if(FitCoreStaticFinalVariable.SUCCESS.equals(contentPredicate.fitCoreEngine(listContent))){
            ((Model)model).addAttribute(String.valueOf(contentName),listContent);
            System.out.println(FitCoreStaticFinalVariable.SUCCESS);
        }
    }
}
