package top.topicsky.www.fitcore.global.util.predicate.judge;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;

import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicVariable.FITFALSE;
import static top.topicsky.www.fitcore.global.resource.constant.eternalpublic.FitCoreEternalPublicVariable.FITTRUE;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.predicate.judge
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 04 日 13 时 49 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicJudgePredicateImpl implements Serializable{
    /**
     * Judge null predicate boolean.
     *
     * @param o
     *         the o
     *
     * @return the boolean
     */
    public static Boolean judgeNullPredicate(Object o){
        if(o==null){
            return FITFALSE;
        }else{
            return FITTRUE;
        }
    }

    /**
     * Judge size zero predicate boolean.
     *
     * @param o
     *         the o
     *
     * @return the boolean
     */
    public static Boolean judgeSizeZeroPredicate(Object o){
        if(((Collection)o).size()==0){
            return FITFALSE;
        }else{
            return FITTRUE;
        }
    }

    /**
     * Judge length zero predicate boolean.
     *
     * @param o
     *         the o
     *
     * @return the boolean
     */
    public static Boolean judgeLengthZeroPredicate(Object o){
        if(((Object[])o).length==0){
            return FITFALSE;
        }else{
            return FITTRUE;
        }
    }
}
