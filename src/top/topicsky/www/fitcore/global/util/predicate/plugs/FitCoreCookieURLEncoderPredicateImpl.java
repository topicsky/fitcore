package top.topicsky.www.fitcore.global.util.predicate.plugs;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.predicate.plugs
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 13 日 14 时 10 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreCookieURLEncoderPredicateImpl implements Serializable{
    /**
     * Cookie encoder string.
     *
     * @param codeType
     *         the code type
     * @param args1
     *         the args 1
     * @param args2
     *         the args 2
     *
     * @return the string
     */
    public static String cookieEncoder(String codeType,String args1,String args2){
        String returnCookieString=null;
        try{
            String arrayTemp_Hx=URLEncoder.encode(args1,codeType);
            String arrayTemp_Hy=URLEncoder.encode(args2,codeType);
            returnCookieString=(arrayTemp_Hx+"=="+arrayTemp_Hy);
        }catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        return returnCookieString;
    }

    /**
     * Cookie decoder string.
     *
     * @param codeType
     *         the code type
     * @param args1
     *         the args 1
     * @param args2
     *         the args 2
     *
     * @return the string
     */
    public static String cookieDecoder(String codeType,String args1,String args2){
        String returnCookieString=null;
        try{
            String arrayTemp_Hx=URLDecoder.decode(args1,codeType);
            String arrayTemp_Hy=URLDecoder.decode(args2,codeType);
            returnCookieString=(arrayTemp_Hx+"=="+arrayTemp_Hy);
        }catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        return returnCookieString;
    }
}
