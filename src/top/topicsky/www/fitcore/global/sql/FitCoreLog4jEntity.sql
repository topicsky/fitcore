-- auto Generated on 2017-06-07 17:24:35 
-- DROP TABLE IF EXISTS `fit_core_log4j_entity`; 
CREATE TABLE `fit_core_log4j_entity`(
    `fit_core_stamp` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'fitCore_stamp',
    `fit_core_thread` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'fitCore_thread',
    `fit_core_info_level` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'fitCore_info_level',
    `fit_core_class` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'fitCore_class',
    `fit_core_message` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'fitCore_message',
    PRIMARY KEY (`fit_core_stamp`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`fit_core_log4j_entity`';
