package top.topicsky.www.fitcore.global.resource.constant.eternalpublic;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * The type Fit core eternal public week.
 */
@Component
@Transactional
public class FitCoreEternalPublicWeek implements Serializable{
    /**
     * The constant SUNDAY.
     */
    public static final String SUNDAY="SUNDAY";
    /**
     * The constant MONDAY.
     */
    public static final String MONDAY="MONDAY";
    /**
     * The constant TUESDAY.
     */
    public static final String TUESDAY="TUESDAY";
    /**
     * The constant WEDNESDAY.
     */
    public static final String WEDNESDAY="WEDNESDAY";
    /**
     * The constant THURSDAY.
     */
    public static final String THURSDAY="THURSDAY";
    /**
     * The constant FRIDAY.
     */
    public static final String FRIDAY="FRIDAY";
    /**
     * The constant SATURDAY.
     */
    public static final String SATURDAY="SATURDAY";
    /**
     * The constant SUNDAYC.
     */
    public static final String SUNDAYC="星期日";
    /**
     * The constant MONDAYC.
     */
    public static final String MONDAYC="星期一";
    /**
     * The constant TUESDAYC.
     */
    public static final String TUESDAYC="星期二";
    /**
     * The constant WEDNESDAYC.
     */
    public static final String WEDNESDAYC="星期三";
    /**
     * The constant THURSDAYC.
     */
    public static final String THURSDAYC="星期四";
    /**
     * The constant FRIDAYC.
     */
    public static final String FRIDAYC="星期五";
    /**
     * The constant SATURDAYC.
     */
    public static final String SATURDAYC="星期六";
}
