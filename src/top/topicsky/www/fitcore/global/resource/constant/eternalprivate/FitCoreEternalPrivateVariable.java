package top.topicsky.www.fitcore.global.resource.constant.eternalprivate;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.resource.constant.eternalinterface
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 22 日 17 时 42 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FitCoreEternalPrivateVariable implements Serializable{
    private static final Boolean FITTRUE=true;
    private static final Boolean FITFALSE=false;
    private static final String SUCCESS="SUCCESS";
    private static final String FAILURE="FAILURE";
    private static final String INITVOIDSTATIC="INITVOIDSTATIC";
    private static final String INITBASICSTATIC="INITBASICSTATIC";
    private static final String INITNOPARAMSTATIC="INITNOPARAMSTATIC";
    private static final String INITNORETURNSTATIC="INITNORETURNSTATIC";
    private static final String INITVOIDDAFAULT="INITVOIDDAFAULT";
    private static final String INITBASICDAFAULT="INITBASICDAFAULT";
    private static final String INITNOPARAMDAFAULT="INITNOPARAMDAFAULT";
    private static final String INITNORETURNDAFAULT="INITNORETURNDAFAULT";
}
